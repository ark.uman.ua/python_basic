#Задача 3. Сумма и разность

def summa_number(number):
    summa = 0
    while number > 0:
        last_digits = number % 10
        summa += last_digits
        number //= 10
    print('\nСумма чисел: ', summa)
    return summa

def count_number(number):
    count_number = 0
    while number > 0:
        count_number += 1
        number = number // 10
    print('Количество цифр в числе: ', count_number)
    return count_number

number = int(input('Введите число: '))
print('Разность суммы и количества цифр: ', summa_number(number) - count_number(number))