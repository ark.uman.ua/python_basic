#Задача 7. Годы

def tree_digits_year(first_year, second_year):
    for i in range(first_year, second_year + 1):
        temp_number = i
        digit_4 = temp_number % 10
        temp_number //= 10
        digit_3 = temp_number % 10
        temp_number //= 10
        digit_2 = temp_number % 10
        temp_number //= 10
        digit_1 = temp_number
        if digit_1 == digit_2 == digit_3 or digit_1 == digit_2 == digit_4 or digit_1 == digit_3 == digit_4 or digit_2 == digit_3 == digit_4:
            print(i)

first_year = int(input('Введите первый год: '))
second_year = int(input('Введите второй год: '))
print('Годы от', first_year, 'до', second_year, 'с тремя одинаковыми цифрами:')
tree_digits_year(first_year, second_year)
