class Steck:
    def __init__(self):
        self.__st = []

    def __str__(self):
        return '; '.join(self.__st)

    def push(self, elem):
        self.__st.append(elem)

    def pop(self):
        if len(self.__st) == 0:
            return None
        return self.__st.pop()


class TaskManager:
    def __init__(self):
        self.task = {}

    def __str__(self):
        result = '\nРезультат:\n'
        if self.task:
            for i_priority in sorted(self.task.keys()):
                result += f'{i_priority}: {self.task[i_priority]}\n'
        return result

    def check(self, task):
        for i_priority in sorted(self.task.keys()):
            if task in str(self.task[i_priority]):
                print('Такая задача уже существует')
                return False
        return True

    def new_task(self, task, priority):
        if self.check(task.lower()):
            if priority not in self.task:
                self.task[priority] = Steck()
            self.task[priority].push(task.lower())

    def del_task(self, priority):
        if priority in self.task.keys():
            self.task[priority].pop()


manager = TaskManager()
manager.new_task('сделать уборку', 4)
manager.new_task('помыть посуду', 4)
manager.new_task('отдохнуть', 1)
manager.new_task('поесть', 2)
manager.new_task('сдать дз', 2)
manager.new_task('покосить траву', 3)
manager.new_task('почистить кулер', 5)
print(manager)
manager.del_task(1)
manager.del_task(4)
manager.new_task('поспать', 1)
manager.del_task(4)
print(manager)

