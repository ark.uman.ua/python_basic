
class Property:
    """
    Базовый класс, описывающий Имущество

    Args:
        worth(int): передается стоимость имущества
    """

    def __init__(self, worth):
        self.worth = worth

    def tax(self):
        """
        Метод для вычисления налога

        :return: ничего не возвращает
        """
        pass


class Apartment(Property):
    """ Класс Квартира, дочерний класс Имущества """

    def __init__(self, worth): super().__init__(worth)

    def tax(self):
        """
        Метод для вычисления налога на квартиру

        :return: возвращает сумму налога на квартиру
        """
        return self.worth / 1000


class Car(Property):
    """ Класс Автомобиль, дочерний класс Имущества """

    def __init__(self, worth): super().__init__(worth)

    def tax(self):
        """
        Метод для вычисления налога на машину

        :return: возвращает сумму налога на машину
        """
        return self.worth / 200


class CountryHouse(Property):
    """ Класс Загородный дом, дочерний класс Имущества """

    def __init__(self, worth): super().__init__(worth)

    def tax(self):
        """
        Метод для вычисления налога на загородного дома

        :return: возвращает сумму налога на загородного дома
        """
        return self.worth / 500


while True:
    setProperty = int(input(
        '\nВыберите интересующее вас имущество:\n'
        '1. Квартира\n'
        '2. Машина\n'
        '3. Загородный дом\n'
        '0. Выход>.\n\n>/: '
    ))

    if setProperty in (1, 2, 3):
        moneyCount = int(input('Введите количество денег: '))
        costProperty = int(input('Введите стоимость имущества: '))

        if setProperty == 1:
            count_tax = Apartment(costProperty)
        elif setProperty == 2:
            count_tax = Car(costProperty)
        else:
            count_tax = CountryHouse(costProperty)

    elif setProperty == 0:
        print('Завершаем программу!')
        break
    else:
        raise ValueError('Выберите правильное значение!')

    print('Налог составляет:', count_tax.tax(),
          'Нужно еще:', '-' if moneyCount - count_tax.tax() >= 0 else count_tax.tax() - moneyCount, '\n')