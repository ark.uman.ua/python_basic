import random


def get_total_food(House):
    return House.total_food


def get_total_profit(House):
    return House.total_profit


def get_count_fur_coat(House):
    return House.count_fur_coat


class House:
    total_food, total_profit, count_fur_coat = 50, 0, 0

    def __init__(self):
        self.__food, self.__food_cat, self.__money, self.__trash = 50, 30, 100, 0

    def set_food(self, food):
        if self.__food < 300:
            self.__food += food
            House.set_total_food(food)
        else:
            raise Exception('В холодильнике нет места')

    def set_food_cat(self, food_cat):
        if self.__food_cat < 100:
            self.__food_cat += food_cat
        else:
            raise Exception('В кладовке нет места')

    def set_money(self, money):
        self.__money += money

    def set_trash(self, trash):
        self.__trash += trash

    def get_food(self):
        return self.__food

    def get_food_cat(self):
        return self.__food_cat

    def get_money(self):
        return self.__money

    def get_trash(self):
        return self.__trash

    def set_total_food(num):
        House.total_food += num

    def set_total_profit(num):
        House.total_profit += num

    def set_count_fur_coat(count):
        House.count_fur_coat += count

    def __str__(self):
        return f'В доме еды: {self.get_food()}, еды для кота: {self.get_food_cat()},' \
               f' денег: {self.get_money()}, мусора: {self.get_trash()}\n'


class Tenant:
    def __init__(self, name, house):
        self.name = name
        self.house = house
        self.__satiety = 30
        self.__happiness = 100

    def set_satiety(self, satiety):
        if self.__satiety < 70:
            self.__satiety += satiety
        else:
            raise Exception('Запредельная сытость')

    def set_happiness(self, happiness):
        self.__happiness += happiness

    def get_satiety(self):
        return self.__satiety

    def get_happiness(self):
        return self.__happiness

    def eat(self):
        self.set_satiety(30), self.house.set_food(-30), print(f'{self.name} поел')

    def pet_cat(self):
        self.set_satiety(-10), self.set_happiness(5), print(f'{self.name} гладил кота')

    def daid(self):
        self.set_satiety(-100), print(f'{self.name} умер')

    def __str__(self):
        return f'{self.name}: сытость: {self.get_satiety()}, счастье: {self.get_happiness()}'


class Husbend(Tenant):
    def __init__(self, name, house):
        super().__init__(name, house)

    def work(self):
        self.set_satiety(-10), self.house.set_money(150), House.set_total_profit(150), self.set_happiness(-15)
        print(f'{self.name} работал')

    def play(self):
        self.set_satiety(-10), self.set_happiness(20)
        print(f'{self.name} играл на копьютере')

    def live_one_day(self):
        if self.get_satiety() <= 30:
            self.eat()
        elif self.house.get_money() < 500:
            self.work()
        else:
            self.play()
        if self.house.get_trash() > 90:
            self.set_satiety(-10)
        if self.get_satiety() < 0 or self.get_satiety() < 10:
            self.daid()
            return False
        else:
            return True


class Wife(Tenant):

    def __init__(self, name, house):
        super().__init__(name, house)

    def buy_products(self):
        self.set_satiety(-10), self.house.set_food(150), self.house.set_money(-150), self.set_happiness(-1)
        print(f'{self.name} купила еды')

    def buy_fur_coat(self):
        self.set_satiety(-10), self.house.set_money(-350), self.set_happiness(60), House.set_count_fur_coat(1)
        print(f'{self.name} купила шубу')

    def buy_food_cat(self):
        self.set_satiety(-10), self.house.set_food_cat(50), self.house.set_money(-50), self.set_happiness(-1)
        House.set_total_food(50), print(f'{self.name} купила еды для кота')

    def cleaning(self):
        self.set_satiety(-10), self.house.set_trash(-80), self.set_happiness(-5)
        print(f'{self.name} убирала дом')

    def live_one_day(self):
        if self.get_satiety() <= 30:
            self.eat()
        if self.get_happiness() < 30 and self.house.get_money() > 500:
            self.buy_fur_coat()
        if self.house.get_food() < 150 < self.house.get_money():
            self.buy_products()
        elif self.house.get_trash() >= 80:
            self.cleaning()
        elif self.house.get_food_cat() < 30:
            self.buy_food_cat()
        if self.house.get_trash() > 90:
            self.set_satiety(-10)
        if self.get_satiety() < 0 or self.get_happiness() < 10:
            self.daid()
            return False
        else:
            return True


class Children(Tenant):
    def __init__(self, name, house):
        super().__init__(name, house)

    def eat(self):
        self.set_satiety(15), self.set_happiness(5), self.house.set_food(-15), print(f'{self.name} поел')

    def play(self):
        self.set_satiety(-3), self.set_happiness(-1), self.house.set_trash(1), print(f'{self.name} играл')

    def live_one_day(self):
        self.play()
        if self.get_satiety() <= 15:
            self.eat()
        elif self.get_happiness() < 10:
            self.pet_cat()
        if self.house.get_trash() > 90:
            self.set_satiety(-10)
        if self.get_satiety() < 0 or self.get_happiness() < 10:
            return False
        else:
            return True


class Cat(Tenant):
    def __init__(self, name, house,):
        super().__init__(name, house)
        self.__satiety = 30

    def eat(self):
        self.set_satiety(20), self.house.set_food_cat(-10), print(f'Кот {self.name} поел')

    def slip(self):
        self.set_satiety(-10), self.set_happiness(-1), print(f'Кот {self.name} спит')

    def tear_wallpaper(self):
        self.house.set_trash(5), self.set_satiety(-5), print(f'{self.name} подрал обои')

    def live_one_day(self):
        if self.get_satiety() < 21:
            self.eat()
        else:
            self.slip()
        if random.randint(1, 3) == 2:
            self.tear_wallpaper()
        if self.get_satiety() < 0 and self.house.get_food_cat < 0:
            return False
        else:
            return True

    def __str__(self):
        return f'{self.name}: сытость: {self.get_satiety()}'


def main(period, house):
    print('*** Поучительная забава - "Давай жить вместе!" *** v.2.0')
    count_day = 1
    corpse = ''
    model_viable = True
    while count_day <= period:
        print(f'----------\nДень {count_day}.')
        print(house)
        for i_member in family:
            if not i_member.live_one_day():
                count_day = period
                corpse = i_member.name
                model_viable = False
        house.set_trash(5)
        count_day += 1
        print('\nСостояние членов семьи на конец дня:')
        for i_member in family:
            print(i_member)
    if model_viable:
        print(f'\nМодель жизнеспособна! Все выжыли\nВ доме осталось денег {house.get_money()}.'
              f'\nЗаработано денег - {get_total_profit(house)}, '
              f'сьедено еды - {get_total_food(house)-house.get_food()}, куплено шуб - {get_count_fur_coat(house)}')
    else:
        print(f'\n{corpse} умер от голода! Модель не жизнеспособна!')


home = House()
adam = Husbend('Адам', home)
eva = Wife('Ева', home)
bob = Children('Боб', home)
vita = Children('Вита', home)
ric = Cat('Рик', home)
family = [adam, eva, bob, vita, ric]

main(365, home)

