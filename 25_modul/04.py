import random


class Person:
    def __init__(self, name, surname, age):
        self.__name = self.set_name(name)
        self.__surname = self.set_surname(surname)
        self.__age = self.set_age(age)

    def set_name(self, name):
        return name

    def set_surname(self, surname):
        return surname

    def set_age(self, age):
        if age in range(18, 90):
            return age
        else:
            raise TypeError('Значение должно быть в диапазоне 18 - 90')

    def get_name(self):
        return self.__name

    def get_surname(self):
        return self.__surname

    def get_age(self):
        return self.__age


class Employee(Person):
    def __init__(self, name, surname, age):
        super().__init__(name, surname, age)

    def salary(self):
        pass


class Manager(Employee):
    position_at_work = 'Менеджер'

    def salary(self):
        return 13000

    def __str__(self):
        return 'Меня зовут {name} {surname}, мне {age}' \
               '\nЯ работаю {position_at_work}' \
               '\nМоя заработная плата составляет {salary}'.format(
            name=self.get_name(),
            surname=self.get_surname(),
            age=self.get_age(),
            position_at_work=self.position_at_work,
            salary=self.salary()

        )


class Agent(Employee):

    position_at_work = 'Агент'
    volume_of_sales = random.randint(10, 100)

    def salary(self):
        return 5000 + self.volume_of_sales * 0.5

    def __str__(self):
        return 'Меня зовут {name} {surname}, мне {age}' \
               '\nЯ работаю {position_at_work}' \
               '\nМоя заработная плата составляет {salary}'.format(
            name=self.get_name(),
            surname=self.get_surname(),
            age=self.get_age(),
            position_at_work=self.position_at_work,
            salary=self.salary()

        )


class Worker(Employee):
    position_at_work = 'Штатный работник'
    hours_worked = random.randint(100, 500)

    def salary(self):
        return 100 * self.hours_worked

    def __str__(self):
        return 'Меня зовут {name} {surname}, мне {age}' \
               '\nЯ работаю {position_at_work}' \
               '\nМоя заработная плата составляет {salary}'.format(
            name=self.get_name(),
            surname=self.get_surname(),
            age=self.get_age(),
            position_at_work=self.position_at_work,
            salary=self.salary()

        )


manager_1 = Manager(name='Artem', surname='Arkon', age=random.randint(18, 90))
manager_2 = Manager(name='Nana', surname='Nimodf', age=random.randint(18, 90))
manager_3 = Manager(name='Fedor', surname='Formula', age=random.randint(18, 90))

agent_1 = Agent(name='Sasha', surname='Pavlov', age=random.randint(18, 90))
agent_2 = Agent(name='Samuel', surname='Fomin', age=random.randint(18, 90))
agent_3 = Agent(name='Lena', surname='Kuka', age=random.randint(18, 90))

worker_1 = Worker(name='Sper', surname='Fkkoe', age=random.randint(18, 90))
worker_2 = Worker(name='Asha', surname='Potato', age=random.randint(18, 90))
worker_3 = Worker(name='Fil', surname='Voron', age=random.randint(18, 90))

print('\t\t\t\n************* Менеджеры *************\n', manager_1, '\n\n', manager_2, '\n\n',  manager_3)
print('\t\t\t\n************* Агенты *************\n', agent_1, '\n\n', agent_2, '\n\n',  agent_3)
print('\t\t\t\n************* Работники *************\n', worker_1, '\n\n', worker_2, '\n\n',  worker_3)


worker_list = [manager_1,  manager_2, manager_3, agent_1, agent_2, agent_3, worker_1, worker_2, worker_3]

for worker in worker_list:
    print(worker, '\n\n')
