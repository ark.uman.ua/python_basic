import math


class Auto:
    def __init__(self, x, y, angle):
        self.set_x(x), self.set_y(y), self.set_angle(angle)

    def set_x(self, x):
        self.__x = x

    def set_y(self, y):
        self.__y = y

    def set_angle(self, angle):
        if angle > 360:
            raise ValueError('Ошибка: угол больше 360')
        else:
            self.__angle = angle

    def get_x(self):
        return self.__x

    def get_y(self):
        return self.__y

    def get_angle(self):
        return self.__angle

    def move(self, angle, distance):
        self.set_x(round(self.get_x() + distance * math.sin(math.radians(angle)), 2))
        self.set_y(round(self.get_y() + distance * math.cos(math.radians(angle)), 2))
        if angle != self.get_angle():
            self.set_angle(angle)

    def __str__(self):
        return 'текущие координаты: {}, {}, угол {}'.format(self.get_x(), self.get_y(), self.get_angle())


class Bus(Auto):
    def __init__(self, x, y, angle, passenger_count=0, money=0):
        super().__init__(x, y, angle)
        self.set_passenger_count(passenger_count), self.set_money(money)

    def set_passenger_count(self, passenger_count):
        if passenger_count < 0:
            raise ValueError('Ошибка: к-во пассажиров не может быть отрицательным')
        else:
            self.__passenger_count = passenger_count

    def set_money(self, money):
        self.__money = money

    def get_passenger_count(self):
        return self.__passenger_count

    def get_money(self):
        return self.__money

    def input(self, people_count):
        if people_count > 0:
            self.set_passenger_count(self.get_passenger_count()+people_count)
        else:
            raise ValueError('Ошибка: к-во пассажиров не может быть отрицательным')

    def exit(self, people_count):
        if people_count <= self.get_passenger_count():
            self.set_passenger_count(self.get_passenger_count()-people_count)
        else:
            raise ValueError('Ошибка: выходит к-во пассажиров больше чем находится в автобусе')

    def move(self, angle, distance):
        super().move(angle, distance)
        self.set_money(self.get_money() + self.get_passenger_count() * distance * 12)

    def __str__(self):
        return '* текущие координаты: {}, {}, угол {} // пассажиров: {}, денег: {}'.format(
            self.get_x(), self.get_y(), self.get_angle(), self.get_passenger_count(), self.get_money())


bus_1 = Bus(0, 0, 0)
bus_1.input(2)
bus_1.move(45, 10)
print(bus_1)
bus_1.input(5)
bus_1.move(135, 10)
print(bus_1)
bus_1.input(10)
bus_1.exit(3)
bus_1.move(225, 10)
print(bus_1)
bus_1.exit(10)
bus_1.move(315, 10)
bus_1.exit(4)
print(bus_1)
