class MyDict(dict):
    def __init__(self):
        super().__init__()

    def get(self, key):
        if key not in self.keys():
            return 0
        else:
            return self[key]

vvgg

d_1 = MyDict()
d_1[1] = 1010
d_1[21] = '2020bfg'
d_1[30] = 3030

print(d_1.get(1))
print(d_1.get(21))
print(d_1.get(10))
