import random
import os


class My_Exception(Exception):
    """ Класс Мои ошибки, описывающий ошибки. Родитель: Exception """

    pass


class KillError(My_Exception):
    """ Класс Ошибка - побит. Родитель: My_Exception"""

    def __str__(self):
        return 'Побит бандитами'


class DrunkError(My_Exception):
    """ Класс Ошибка - папился. Родитель: My_Exception"""

    def __str__(self):
        return 'Напился'


class CarCrashError(My_Exception):
    """ Класс Ошибка - автомобильная авария. Родитель: My_Exception"""

    def __str__(self):
        return 'Попал в ДПТ'


class GluttonyError(My_Exception):
    """ Класс Ошибка - отравился. Родитель: My_Exception """

    def __str__(self):
        return 'Отривался щами'


class DepressionError(My_Exception):
    """ Класс Ошибка - побит. Родитель: My_Exception"""

    def __str__(self):
        return 'Посмотрел Киселёва'


class Day:
    """
    Класс День, описывающий один прожитый день.

    Args:
        ball (int): единицы кармы

    Attributes:
        counter (int): счётчик дней
    """
    __counter = 0
    karma_log = ''

    def __init__(self):
        self.__ball = 0

    def get_counter(self):
        """
        Геттер счетчика дней.

        :return: __count
        :type:      int
        """
        return Day.__counter

    def get_ball(self):
        """
        Геттер счетчика единиц кармы.

        :return: __ball
        :type:     int
        """
        return self.__ball

    def set_ball(self, ball):
        """
        Сеттер счетчика кармы.

        :param: ball: единица кармы
        :tupe:  ball: int
        """
        self.__ball += ball

    def one_day(self):
        """
        Метод - Прожить один день.

        :return: случайное число от 1 до 7 включительно:type:  int
        :raise:  случайная ошибка из списка [KillError(), DrunkError(), CarCrashError(),
                GluttonyError(), DepressionError()], выпадающая с вероятностью 1 к 10.
                При выпадании ошибки, в файл karma.log идет запись об ошибке
        """
        Day.__counter += 1
        try:
            if random.randint(1, 10) == 10:
                raise random.choice([KillError(), DrunkError(), CarCrashError(), GluttonyError(), DepressionError()])
        except (KillError, DrunkError, CarCrashError, GluttonyError, DepressionError) as err:
            Day.karma_log += f'{self.get_counter()} день. {err}\n'
        finally:
            return random.randint(1, 7)


def main(period):
    """
    Основной метод, запускающий программу.
    Производится вывод в консоль достигнутых очков кармы и к-ва дней их достижения
    """
    if os.path.exists('karma.log'):
        os.remove('karma.log')
    while period.get_ball() < 500:
        period.set_ball(period.one_day())
    with open('karma.log', 'a+', encoding='utf-8') as file:
        file.write(Day.karma_log)
    print('{} очков кармы.\nЧтобы достичь просветления понадобилось {} дней'.
          format(period.get_ball(), period.get_counter()))


day = Day()
main(day)
