# Задача 5. Текстовый калькулятор
# Иван стоит на пороге величайшего открытия (не будем его расстраивать), которое перевернёт представление о всей
# математике и программировании. Имя этому открытию — текстовый калькулятор. Правда, код для своего открытия ему сложно
# написать самому, и поэтому он попросил вас помочь ему. Так что уже можно бежать в патентное бюро.
# Есть файл calc.txt, в котором хранятся записи вида
# 100 + 34
# 23 / 4
# то есть ОПЕРАНД_1 ОПЕРАЦИЯ ОПЕРАНД_2, разделённые пробелами.
# Операнды — целые числа. Операции — арифметические (включая деление нацело и нахождение остатка). Напишите программу,
# которая вычисляет все эти операции и находит сумму их результатов. Пропишите обработку возможных ошибок. Программа не
# должна завершаться при первой же ошибке, она учитывает все верные строки и выводит найденный ответ.


def validation_data(string):
    tmp_list = string.split()
    if len(tmp_list) != 3:
        raise IndexError
    elif not (tmp_list[0].isdigit() and tmp_list[2].isdigit()):
        raise ValueError
    elif not tmp_list[1] in ('+', '-', '/', '*', '//', '%', '**'):
        raise SyntaxError
    elif tmp_list[2] == '0':
        raise ZeroDivisionError
    else:
        return True


def action(string, result):
    tmp_list = string.split()
    if tmp_list[1] == '+':
        return result.append(int(tmp_list[0]) + int(tmp_list[2]))
    elif tmp_list[1] == '-':
        return result.append(int(tmp_list[0]) - int(tmp_list[2]))
    elif tmp_list[1] == '*':
        return result.append(int(tmp_list[0]) * int(tmp_list[2]))
    elif tmp_list[1] == '/':
        return result.append(int(tmp_list[0]) / int(tmp_list[2]))
    elif tmp_list[1] == '//':
        return result.append(int(tmp_list[0]) // int(tmp_list[2]))
    elif tmp_list[1] == '%':
        return result.append(int(tmp_list[0]) % int(tmp_list[2]))
    elif tmp_list[1] == '**':
        return result.append(int(tmp_list[0]) ** int(tmp_list[2]))
    else:
        print('Операция невозможна')


results = list()
with open('calc.txt', 'r') as file:
    for i_line in file:
        try:
            if validation_data(i_line):
                action(i_line, results)
        except (IndexError, ValueError, SyntaxError, ZeroDivisionError) as err:
            print('Ошибка: невозможно выполнить операцию')
print(f'Сумма результатов: {sum(results)}')
