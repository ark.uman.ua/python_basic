# Задача 1. Имена 2
# Есть файл people.txt, в котором построчно хранится N имён пользователей.
# Напишите программу, которая берёт количество символов в каждой строке файла и в качестве ответа выводит общую сумму.
# Если в какой-либо строке меньше трёх символов (не считая литерала \n), то вызывается ошибка и сообщение, в какой
# именно строке ошибка возника. Программа при этом не завершается и обрабатывает все имена файла.
# Также при желании можно вывести все ошибки в отдельный файл errors.log.

import os

sum_sym = 0
line_count = 0
try:
    with open('people.txt', 'r', encoding='utf-8') as file_data:
        for i_line in file_data:
            line_count += 1
            try:
                if len(''.join(i_line.split())) < 3:
                    raise ValueError
            except ValueError:
                print(f'Ошибка: в строке {line_count} меньше трёх символов')
                with open('errors.log', 'a+', encoding='utf-8') as file:
                    file.write(f'Ошибка: в строке {line_count} меньше трёх символов\n')
                continue
            sum_sym += len(''.join(i_line.split()))
except FileNotFoundError:
    print('Ошибка: файл не найден')
finally:
    print('\nНайденая сумма символов: ', sum_sym)

if os.path.exists('errors.log'):
    answer = input('\nСохранить все ошибки в отдельный файл errors.log?  ')
    if answer == 'да':
        print('Лог сохранен.')
    else:
        os.remove('errors.log')





