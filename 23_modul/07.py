def validation_data(string):
    tmp_list = string.split()
    if len(tmp_list) != 3:
        raise IndexError
    elif not (tmp_list[0].isdigit() and tmp_list[2].isdigit()):
        raise ValueError
    elif not tmp_list[1] in ('+', '-', '/', '*', '//', '%', '**'):
        raise SyntaxError
    elif tmp_list[2] == '0':
        raise ZeroDivisionError
    else:
        return True


def action(string, result):
    tmp_list = string.split()
    if tmp_list[1] == '+':
        return result.append(int(tmp_list[0]) + int(tmp_list[2]))
    elif tmp_list[1] == '-':
        return result.append(int(tmp_list[0]) - int(tmp_list[2]))
    elif tmp_list[1] == '*':
        return result.append(int(tmp_list[0]) * int(tmp_list[2]))
    elif tmp_list[1] == '/':
        return result.append(int(tmp_list[0]) / int(tmp_list[2]))
    elif tmp_list[1] == '//':
        return result.append(int(tmp_list[0]) // int(tmp_list[2]))
    elif tmp_list[1] == '%':
        return result.append(int(tmp_list[0]) % int(tmp_list[2]))
    elif tmp_list[1] == '**':
        return result.append(int(tmp_list[0]) ** int(tmp_list[2]))
    else:
        print('Операция невозможна')


results = list()
with open('calc.txt', 'r') as file:
    for i_line in file:
        while True:
            try:
                if validation_data(i_line):
                    action(i_line, results)
                    break
            except (IndexError, ValueError, SyntaxError, ZeroDivisionError) as err:
                answer = input(f'Ошибка в строке: {i_line[:len(i_line)-1]} Хотите исправить? ')
                if answer == 'да':
                    i_line = input('Введите исправленную строку: ')+'\n'
                else:
                    break
print(f'Сумма результатов: {sum(results)}')
