# Задача 3. Счастливое число
# Напишите программу, которая запрашивает у пользователя число до тех пор, пока сумма этих чисел не станет больше либо
# равна 777. Каждое введённое число при этом дозаписывается в файл. Сделайте так, чтобы перед дозаписью программа с
# вероятностью 1 к 13 выбрасывала пользователю случайное исключение и завершалась.

import random

summa = 0
errors_list = [NameError, SyntaxError, ValueError, IndexError]
try:
    with open('out_file.txt', 'w+') as file:
        while summa <= 777:
            user_num = input('Введите число: ')
            try:
                summa += float(user_num)
                if random.randint(1, 13) == 13:
                    print('Вас постигла неудача!')
                    raise random.choice(errors_list)
                file.write(user_num+'\n')
            except ValueError:
                print('Ошибка: введено не число, попробуйте еще раз')
    print('Вы успешно выполнили условие для выхода из порочного цикла!')
finally:
    print('\nСодержимое файла out_file.txt:')
    with open('out_file.txt', 'r') as file:
        print(file.read())
