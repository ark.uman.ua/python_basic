# Задача 4. Регистрация
# У вас есть файл с протоколом регистраций пользователей на сайте — registrations.txt
# Каждая строка содержит: ИМЯ ИМЕЙЛ ВОЗРАСТ, разделённые пробелами.
# # Например:
# # Василий test@test.ru 27
# Напишите программу, которая проверяет данные из файла для каждой строки:
# Присутствуют все три поля.
# Поле имени содержит только буквы.
# Поле «Имейл» содержит @ и . (точку).
# Поле «Возраст» является числом от 10 до 99.
# В результате проверки сформируйте два файла:
# registrations_good.log — для правильных данных, записывать строки как есть,
# registrations_bad.log — для ошибочных, записывать строку и вид ошибки.
# Для валидации строки данных напишите функцию, которая может выдавать исключения:
# НЕ присутствуют все три поля: IndexError.
# Поле имени содержит НЕ только буквы: NameError.
# Поле «Имейл» НЕ содержит @ и .(точку): SyntaxError.
# Поле «Возраст» НЕ является числом от 10 до 99: ValueError.

def validation_data(string):
    tmp_list = string.split()
    if len(tmp_list) < 3:
        raise IndexError('НЕ присутствуют все три поля')
    elif not tmp_list[0].isalpha():
        raise NameError('Поле имени содержит НЕ только буквы')
    elif tmp_list[1].find('@') == -1 or tmp_list[1].find('.') == -1:
        raise SyntaxError('Поле «Имейл» НЕ содержит @ и .(точку)')
    elif not 10 <= int(tmp_list[2]) <= 99:
        raise ValueError('Поле «Возраст» НЕ является числом от 10 до 99')
    else:
        return True


with open('registrations_good.log', 'w+', encoding='utf-8') as file_good:
    with open('registrations_bad.log', 'w+', encoding='utf-8') as file_bad:
        with open('registrations.txt', 'r', encoding='utf-8') as file:
            for i_line in file:
                try:
                    if validation_data(i_line):
                        file_good.write(i_line)
                except (IndexError, NameError, SyntaxError, ValueError, TypeError) as err:
                    file_bad.write(f'{i_line[:len(i_line)-1]}\t\t\t{err}\n')
