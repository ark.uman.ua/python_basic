# Задача 2. Самое длинное слово
# Дана строка, содержащая пробелы. Найдите в ней самое длинное слово, выведите  это слово и его длину.
# Если таких слов несколько, выведите первое из них.

text = input('Введите текст: ').split()
word_max_letter = 0
word_max_list = []

for i_word in text:
    if len(i_word) > word_max_letter:
        word_max_list.insert(0, i_word)
        word_max_letter = len(i_word)

print('Самое длинное слово: ', word_max_list[0], '\nего длина: ', word_max_letter, 'букв')

