# Задача 8. Бегущая строка
# В одном из домашних заданий мы писали для табло программу, которая циклически сдвигает элементы списка чисел вправо
# на K позиций. В этот раз мы работаем с двумя строками, и нам нужно проверить, не равна ли на самом деле одна другой.
# Возможно, одна из них просто немного сдвинута. Пользователь вводит две строки. Напишите программу, которая определяет,
# можно ли первую строку получить из второй
# циклическим сдвигом.
# Опционально: если получить можно, то выведите значение этого сдвига.
# Пример 1:
# Первая строка: abcd
# Вторая строка: cdab
# Первая строка получается из второй со сдвигом 2.
# Пример 2:
# Первая строка: abcd
# Вторая строка: cdba
# Первую строку нельзя получить из второй с помощью циклического сдвига.

fst_str = input('Первая строка: ')
scd_str = input('Вторая строка: ')

if len(fst_str) != len(scd_str):
    print('Строки разные по длинне')
else:
    step = scd_str.find(fst_str[0])
    check = False
    for i_sym in range(len(fst_str)):
        if fst_str[i_sym] == scd_str[(i_sym+step) % len(fst_str)]:
            check = True
        else:
            check = False
            break
    if check:
        print('Первая строка получается из второй со сдвигом ', step)
    else:
        print('Первую строку нельзя получить из второй с помощью циклического сдвига.')
