# Задача 6. Сжатие списка
# Дан список из N целых чисел. Напишите программу, которая выполняет «сжатие списка» — переставляет все нулевые элементы
# в конец массива. При этом все ненулевые элементы располагаются в начале массива в том же порядке.
# Затем все нули из списка удаляются.

import random

numbers = [random.randint(-2, 2) for _ in range(int(input('Введите длину списка: ')))]

numbers_sort = [i_num for i_num in numbers if i_num != 0]
numbers_sort.extend([0]*numbers.count(0))

print(numbers)
print(numbers_sort)
print(numbers_sort[:len(numbers_sort)-numbers_sort.count(0)])
