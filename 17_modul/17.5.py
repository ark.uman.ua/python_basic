# Задача 5. Разворот

# На вход в программу подаётся строка, в которой буква h встречается как минимум два раза. Реализуйте код,
# который разворачивает последовательность символов, заключённую между первым и последним появлением буквы h,
# в противоположном порядке.

def num_letter(text):
    for i_let in text:
        if i_let == 'h':
            return text.index(i_let)


print('Enter text that has at least two "h" letters:')
user_text = input('')

print(user_text[:num_letter(user_text)] + user_text[-num_letter(user_text[::-1])-1:num_letter(user_text)-1:-1]
      + user_text[-num_letter(user_text[::-1]):])
