# Задача 5. Гистограмма частоты 2
# Мы уже писали программу для лингвистов, которая получала на вход текст и считала, сколько раз в строке встречается
# каждый символ. Теперь задача немного поменялась: максимальную частоту выводить не нужно, однако теперь необходимо
# написать функцию, которая будет инвертировать полученный словарь. То есть в качестве ключа будет частота, а в качестве
# значения — список символов с этой частотой. Реализуйте такую программу.
# Пример:
# Введите текст: Здесь что-то написано
# Оригинальный словарь частот:
#   : 2
# - : 1
# З : 1
# а : 2
# д : 1
# е : 1
# и : 1
# н : 2
# о : 3
# п : 1
# с : 2
# т : 2
# ч : 1
# ь : 1
# Инвертированный словарь частот:
# 1 : ['З', 'д', 'е', 'ь', 'ч', '-', 'п', 'и']
# 2 : ['с', ' ', 'т', 'н', 'а']
# 3 : ['о']

def histogram(string):
    origin_dict = dict()
    for sym in string:
        if sym in origin_dict:
            origin_dict[sym] += 1
        else:
            origin_dict[sym] = 1
    return origin_dict


def hist_frequency(hist_dict):
    invert_dict = dict()
    for i_k in range(1, len(set(hist_dict.values())) + 1):
        invert_dict[i_k] = [key for key, val in hist_dict.items() if val == i_k]
    return invert_dict


text = input('Введите текст: ')

hist_origin = histogram(text)

print('\nОригинальный словарь частот:')
for i_key in sorted(hist_origin.keys()):
    print(i_key, ':', hist_origin[i_key])

hist_invert = hist_frequency(hist_origin)

print('\nИнвертированный словарь частот:')
for i_key in sorted(hist_invert.keys()):
    print(i_key, ':', hist_invert[i_key])
