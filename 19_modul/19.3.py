# Задача 3. Криптовалюта
# При работе с API (application programming interface) сайта биржи по криптовалюте вы получили вот такие данные в виде словаря:
# data = {
#     "address": "0x544444444444",
#     "ETH": {
#         "balance": 444,
#         "totalIn": 444,
#         "totalOut": 4
#     },
#     "count_txs": 2,
#     "tokens": [
#         {
#             "fst_token_info": {
#                 "address": "0x44444",
#                 "name": "fdf",
#                 "decimals": 0,
#                 "symbol": "dsfdsf",
#                 "total_supply": "3228562189",
#                 "owner": "0x44444",
#                 "last_updated": 1519022607901,
#                 "issuances_count": 0,
#                 "holders_count": 137528,
#                 "price": False
#             },
#             "balance": 5000,
#             "totalIn": 0,
#             "total_out": 0
#         },
#         {
#             "sec_token_info": {
#                 "address": "0x44444",
#                 "name": "ggg",
#                 "decimals": "2",
#                 "symbol": "fff",
#                 "total_supply": "250000000000",
#                 "owner": "0x44444",
#                 "last_updated": 1520452201,
#                 "issuances_count": 0,
#                 "holders_count": 20707,
#                 "price": False
#             },
#             "balance": 500,
#             "totalIn": 0,
#             "total_out": 0
#         }
#     ]
# }
# Теперь вам предстоит немного обработать эти данные.
# Напишите программу, которая выполняет следующий алгоритм действий:
# Вывести списки ключей и значений словаря.
# В “ETH” добавить ключ “total_diff” со значением 100.
# Внутри “fst_token_info” значение ключа “name” поменять с “fdf” на “doge”.
# Удалить “total_out” из tokens и присвоить его значение в “total_out” внутри “ETH”.
# Внутри "sec_token_info" изменить название ключа “price” на “total_price”.

data = dict()

data['address'] = 0x544444444444

data['ETH'] = dict()
data['ETH']['balance'] = 444
data['ETH']['totalIn'] = 444
data['ETH']['totalOut'] = 4

data['count_txs'] = 2

data['tokens'] = list()
data['tokens'].append(dict())
data['tokens'][0]['fst_token_info'] = dict()
data['tokens'][0]['fst_token_info']['address'] = 0x44444
data['tokens'][0]['fst_token_info']['name'] = 'fdf'
data['tokens'][0]['fst_token_info']['decimals'] = 0
data['tokens'][0]['fst_token_info']['symbol'] = 'dsfdsf'
data['tokens'][0]['fst_token_info']['total_supply'] = 3228562189
data['tokens'][0]['fst_token_info']['owner'] = 0x44444
data['tokens'][0]['fst_token_info']['last_updated'] = 1520452201
data['tokens'][0]['fst_token_info']['issuances_count'] = 0
data['tokens'][0]['fst_token_info']['holders_count'] = 20707
data['tokens'][0]['fst_token_info']['price'] = False
data['tokens'][0]['balance'] = 5000
data['tokens'][0]['totalIn'] = 0
data['tokens'][0]['total_out'] = 0

data['tokens'].append(dict())
data['tokens'][1]['sec_token_info'] = dict()
data['tokens'][1]['sec_token_info']['address'] = 0x44444
data['tokens'][1]['sec_token_info']['name'] = 'ggg'
data['tokens'][1]['sec_token_info']['decimals'] = 2
data['tokens'][1]['sec_token_info']['symbol'] = 'fff'
data['tokens'][1]['sec_token_info']['total_supply'] = 250000000000
data['tokens'][1]['sec_token_info']['owner'] = 0x44444
data['tokens'][1]['sec_token_info']['last_updated'] = 1520452201
data['tokens'][1]['sec_token_info']['issuances_count'] = 0
data['tokens'][1]['sec_token_info']['holders_count'] = 20707
data['tokens'][1]['sec_token_info']['price'] = False

data['tokens'][1]['balance'] = 500
data['tokens'][1]['totalIn'] = 0
data['tokens'][1]['total_out'] = 0

print('Списки ключей и значений словаря: ', data)

data['ETH']['total_diff'] = 100
data['tokens'][0]['fst_token_info']['name'] = 'doge'
data['ETH']['totalOut'] = data['tokens'][0].pop('total_out')
data['ETH']['totalOut'] = data['tokens'][1].pop('total_out')
data['tokens'][1]['sec_token_info']['total_price'] = data['tokens'][1]['sec_token_info'].pop('price')

print('Изменённые списки ключей и значений словаря: ', data)

