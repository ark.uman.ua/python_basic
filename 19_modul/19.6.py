# Задача 6. Словарь синонимов
# Одна библиотека поручила вам написать программу для оцифровки словарей слов-синонимов. На вход в программу подаётся
# N пар слов. Каждое слово является синонимом к парному ему слову.
# Реализуйте код, который составляет словарь слов-синонимов (все слова в словаре различны), затем запрашивает у
# пользователя слово и выводит на экран его синоним. Обеспечьте контроль ввода: если такого слова нет, то выведите
# ошибку и запросите слово ещё раз. При этом проверка не должна зависеть от регистра символов.
# Пример:
# Введите количество пар слов: 3
# 1 пара: Привет - Здравствуйте
# 2 пара: Печально - Грустно
# 3 пара: Весело - Радостно
# Введите слово: интересно
# Такого слова в словаре нет.
# Введите слово: здравствуйте
# Синоним: Привет

synonym_dict = dict()

num_pair = int(input('Введите количество пар слов: '))

for i_pair in range(1, num_pair+1):
    print(i_pair, 'пара: ', end='')
    syn_pair = input().split(' - ')
    synonym_dict[syn_pair[0]] = syn_pair[1]

while True:
    word = input('Введите слово: ').title()
    if word in synonym_dict.keys():
        print('Синоним: ', synonym_dict[word])
        break
    elif word in synonym_dict.values():
        print('Синоним: ', ''.join(key for key, val in synonym_dict.items() if word == val))
        break
    else:
        print('Такого слова в словаре нет.')
