
from collections.abc import Iterable
import os.path
import os


def gen_files_path(user_dir, path=os.path.abspath(os.sep), ) -> Iterable[str]:
    for link, dirs, files in list(os.walk(path)):
        if link.split('\\')[-1] == user_dir:
            return
        for file in files:
            yield os.path.join(link, file)


dir_find = input('Имя искомой директории: ')

print('\n-----------------------------\n')
for i in gen_files_path(user_dir=dir_find):
    print(i)

