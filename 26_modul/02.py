from collections.abc import Iterable

list_1 = [2, 5, 7, 10]
list_2 = [3, 8, 4, 9]
find = 56


def to_find(lst1: list, lst2: list, num_find: int) -> Iterable[int]:
    for x in lst1:
        for y in lst2:
            yield x, y, x * y
            if x * y == num_find:
                print('Found!!!')
                return


for i in to_find(lst1=list_1, lst2=list_2, num_find=find):
    print(i)


