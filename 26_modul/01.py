from collections.abc import Iterable


class NumberSquares:
    def __init__(self, number: int) -> None:
        self.__index = number
        self.__counter = 0

    def __next__(self) -> int:
        if self.__counter < self.__index:
            self.__counter += 1
            return self.__counter ** 2
        else:
            raise StopIteration

    def __iter__(self) -> Iterable[int]:
        return self


def number_squares(number: int) -> Iterable[int]:
    for i_num in range(1, number+1):
        yield i_num**2


num = int(input('Введите число: '))

# класс-итератор
my_iter = NumberSquares(num)
for i in my_iter:
    print(i, end=' ')

print('\n**********')
# генераторная функция
my_iter2 = number_squares(number=num)
for i in my_iter2:
    print(i, end=' ')

print('\n**********')
# генераторное выражение
my_iter3 = (i_num**2 for i_num in range(1, num+1))
for i in my_iter3:
    print(i, end=' ')
