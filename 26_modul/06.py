from typing import Any, Optional
from collections.abc import Iterable



class Node:
    def __init__(self, value: Optional[Any] = None, next: Optional['Node'] = None) -> None:
        self.value = value
        self.next = next

    def __str__(self):
        return 'Node [{value}]'.format(value=self.value)


class LinkedList:
    def __init__(self) -> None:
        self.head = None
        self.length = 0

    def __str__(self):
        if self.head is not None:
            current = self.head
            values = [str(current.value)]
            while current.next is not None:
                current = current.next
                values.append(str(current.value))
            return '[{values}]'.format(values=', '.join(values))
        return 'LinkedList []'

    def __iter__(self) -> Iterable[str]:
        current_node = self.head
        while current_node is not None:
            yield '{value}'.format(value=current_node.value)
            current_node = current_node.next

    def append(self, data: Any) -> None:
        new_node = Node(data)
        if self.head is None:
            self.head = new_node
            return
        last = self.head
        while last.next:
            last = last.next
        last.next = new_node
        self.length += 1

    def get(self, index) -> str:
        current_node = self.head
        current_index = 0
        if self.length == 0 or self.length < index:
            raise IndexError
        while current_node is not None:
            if current_index == index:
                return '{value}'.format(value=current_node.value)
            current_node = current_node.next
            current_index += 1

    def remove(self, index) -> None:
        current_node = self.head
        current_index = 0
        if self.length == 0 or self.length <= index:
            raise IndexError
        if current_node is not None:
            if index == 0:
                self.head = current_node.next
                self.length -= 1
                return
        while current_node is not None:
            if current_index == index:
                return
            previous = current_node
            current_node = current_node.next
            current_index += 1
            previous.next = current_node.next
            self.length -= 1


my_list = LinkedList()
my_list.append(10)
my_list.append(20)
my_list.append(30)
my_list.append(40)
print('Текущий список:', my_list)
print('Получение третьего элемента:', my_list.get(2))
print('Удаление второго элемента.')
my_list.remove(1)
print('Новый список:', my_list)

for i_elem in my_list:
    print(i_elem)
