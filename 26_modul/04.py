
#           Q(n)=Q(n−Q(n−1))+Q(n−Q(n−2))
from collections.abc import Iterable


def q_hofstadter(s) -> Iterable[int]:
    sequence = s[:]
    while True:
        try:
            q = sequence[-sequence[-1]] + sequence[-sequence[-2]]
            sequence.append(q)
            yield q
        except IndexError:
            return


counter = 1
for x in q_hofstadter([1, 1]):
    if counter > 10:
        break
    print(x, end=' ')
    counter += 1

