from collections.abc import Iterable
import os.path
import os

total_sum_str_code = 0


def gen_files_path(user_dir=os.path.abspath(os.sep)) -> Iterable[str]:
    for link, dirs, files in os.walk(user_dir):
        for file in files:
            if file.endswith('py'):
                yield os.path.join(link, file)


def get_str_code(files_path):
    for i_file in files_path:
        sum_str_code = 0
        with open(i_file, 'r', encoding='utf-8') as data:
            print('\t', i_file.split('\\')[1], end=' ')
            for i_line in data:
                if len(i_line) > 1 and not i_line.startswith('#'):
                    sum_str_code += 1
            yield sum_str_code


dir_find = input('Путь к директории: ')

print(f'\nДиректория содержит файлы:')

for counter in get_str_code(gen_files_path(dir_find)):
    print(f'   / к-во строк кода: {counter}')
    total_sum_str_code += counter
print(f'Общее количество строк кода: {total_sum_str_code}')




