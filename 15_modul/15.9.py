#Задача 9. Анализ слова 2

word = input('Введите слово: ')
word_list = list(word)

count = 0

for i in range(len(word_list)):
    if word_list[i] != word_list[len(word_list) - 1 - count]:
        print('Слово не является палиндромом')
        break
    else:
        count += 1

if count == len(word_list):
    print('Слово является палиндромом')