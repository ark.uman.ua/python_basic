#Задача 4. Видеокарты

cart_list = []
new_cart_list = []
number_carts = int(input('Количество видеокарт: '))
older_model = 0

for i in range(number_carts):
    print(i+1, 'Видеокарта:', end=' ')
    model_cart = int(input())
    if model_cart >= older_model:
        older_model = model_cart
    cart_list.append(model_cart)

for i in range(number_carts):
    if cart_list[i] != older_model:
        new_cart_list.append(cart_list[i])

print('\nСтарый список видеокарт: ', cart_list)
print('Новый список видеокарт: ', new_cart_list)

