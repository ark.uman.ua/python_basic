#Задача 8. Бегущие цифры

start_list = []
new_list = []

count = int(input('К-во елементов в списке: '))
shift_step = int(input('Сдвиг: '))

for _ in range(count):
    element = int(input('Введите елемент: '))
    start_list.append(element)

for i in range(count):
    new_list.append(start_list[i-shift_step])

print('Изначальный список: ', start_list)
print('Сдвинутый список:', new_list)
