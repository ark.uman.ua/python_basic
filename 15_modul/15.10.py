#Задача 10. Сортировка

import random

def random_filler(n):
    number_list = []
    for _ in range(n):
        number_list.append(random.randint(-100, 100))
    return number_list


n = int(input('К-во чисел в списке: '))
number_list = random_filler(n)

print('\nИзначальный список: ', number_list)

for i in range(n):
    max_num = number_list[i]
    for a in number_list:
        if max_num <= a:
            number_list[i], number_list[number_list.index(a)] = number_list[number_list.index(a)], number_list[i]

print('Отсортированный список: ', number_list)
