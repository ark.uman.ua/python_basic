class Student:
    list_student = []

    def __init__(self, full_name, group_number, progress):
        self.full_name = full_name
        self.group_number = group_number
        self.progress = progress
        self.average = sum(progress) / len(progress)
        self.list_student.append([self.full_name, self.group_number, self.progress, self.average])


st_1 = Student('Чук Гена', '14-к', [4, 5, 3, 5, 2])
st_2 = Student('Кримський Іван', '25', [2, 3, 3, 4, 2])
st_3 = Student('Генералов Нет', '200', [4, 5, 4, 5, 5])
st_4 = Student('Путин Осел', '101', [1, 1, 2, 3, 2])
st_5 = Student('Байден Жук', '45', [4, 4, 3, 5, 1])
st_6 = Student('Жамкало Коля', '30', [4, 5, 5, 5, 3])
st_7 = Student('Сто Пудоф', '3', [5, 5, 5, 5, 5])
st_8 = Student('Все Всем', '41', [4, 4, 3, 4, 3])
st_9 = Student('Дункан Елеонора', '30', [4, 5, 4, 5, 4])
st_10 = Student('Мазай Дед', '40', [4, 4, 4, 4, 5])

Student.list_student.sort(key=lambda student: student[3], reverse=False)
print('\nСписок студентов по среднему баллу (по возрастанию)\n')
print(f'{"Фамилия имя":25}{"Номер группы":15}{"Успеваемость":20}{"средний бал":5}\n')

for average_student in Student.list_student:
    print('{:25}''{:15}''{:20}''{:5}'.format(
        average_student[0], average_student[1], ", ".join(map(str, average_student[2])), average_student[3])
    )

