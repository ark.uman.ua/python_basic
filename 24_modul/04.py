class Parent:

    def __init__(self, name, age, childs):
        self.name, self.age, self.childs = name, age, childs
        for chd in self.childs:
            if self.age - chd.age < 16:
                raise ValueError('Ошибка: возраст родителя слишком мал, либо возраст '
                                 'ребенка слишком велик (разница меньше 16 лет)')

    def __str__(self):
        return f'Родитель {self.name}, возраст {self.age}, дети:\n{"".join(str(c_d) for c_d in self.childs)}'

    def to_appease(self, child):
        if child not in self.childs:
            print(f'У {self.name} нет ребенка с именем {child.name}')
            return
        for i_child in self.childs:
            if i_child is child:
                if i_child.state_calm != 0:
                    i_child.state_calm = 0
                    print(f'{i_child.name} успокоен')
                else:
                    print(f'{i_child.name} сейчас спокоен')

    def to_feed(self, child):
        if child not in self.childs:
            print(f'У {self.name} нет ребенка с именем {child.name}')
            return
        for i_child in self.childs:
            if i_child is child:
                if i_child.state_hunger != 0:
                    i_child.state_hunger = 0
                    print(f'{i_child.name} накормлен')
                else:
                    print(f'{i_child.name} сейчас сыт')
                i_child.state_hunger = 0


class Children:
    status_calm = {0: 'спокойный', 1: 'тревожный'}
    status_satiety = {0: 'сыт', 1: 'голоден'}

    def __init__(self, name, age, state_calm, state_hunger):
        self.name, self.age, self.state_calm, self.state_hunger = name, age, state_calm, state_hunger

    def __str__(self):
        return f'{self.name}, возраст {self.age}, состояние: ' \
               f'{Children.status_calm[self.state_calm]}, {Children.status_satiety[self.state_hunger]}\n'


ivan = Children('Иван', 10, 0, 1)
tanya = Children('Таня', 5, 1, 0)
roma = Children('Рома', 12, 1, 1)
ura = Children('Юра', 6, 0, 0)
anton = Children('Антон', 4, 1, 1)

print(ura, ivan, tanya, roma, anton)

ira = Parent('Ира', 30, [ivan, tanya])
sasha = Parent('Саша', 28, [roma, ura])
petya = Parent('Петя', 20, [anton])

print(sasha, ira, petya)

ira.to_appease(ivan)
sasha.to_feed(ura)
petya.to_feed(anton)
petya.to_appease(ira)
petya.to_appease(anton)
