import random
shoe_box_card = list(range(2, 54))


class Card:
    cards = {
            2: ['двойка пика', 2], 3: ['тройка пика', 3], 4: ['черверка пика', 4], 5: ['пятерка пика', 5],
            6: ['шестерка пика', 6], 7: ['семерка пика', 7], 8: ['весьмерка пика', 8], 9: ['девятка пика', 9],
            10: ['десятка пика', 10], 11: ['валет пика', 10], 12: ['дама пика', 10], 13: ['король пика', 10],
            14: ['туз пика', 11, 1], 15: ['двойка трефа', 2], 16: ['тройка трефа', 3], 17: ['черверка трефа', 4],
            18: ['пятерка трефа', 5], 19: ['шестерка трефа', 6], 20: ['семерка трефа', 7], 21: ['весьмерка трефа', 8],
            22: ['девятка трефа', 9], 23: ['десятка трефа', 10], 24: ['валет трефа', 10], 25: ['дама трефа', 10],
            26: ['король трефа', 10], 27: ['туз трефа', 11, 1], 28: ['двойка чирвы', 2], 29: ['тройка чирвы', 3],
            30: ['черверка чирвы', 4], 31: ['пятерка чирвы', 5], 32: ['шестерка чирвы', 6], 33: ['семерка чирвы', 7],
            34: ['весьмерка чирвы', 8], 35: ['девятка чирвы', 9], 36: ['десятка чирвы', 10], 37: ['валет чирвы', 10],
            38: ['дама чирвы', 10], 39: ['король чирвы', 10], 40: ['туз чирвы', 11, 1], 41: ['двойка бубна', 2],
            42: ['тройка бубна', 3], 43: ['черверка бубна', 4], 44: ['пятерка бубна', 5], 45: ['шестерка бубна', 6],
            46: ['семерка бубна', 7], 47: ['весьмерка бубна', 8], 48: ['девятка бубна', 9], 49: ['десятка бубна', 10],
            50: ['валет бубна', 10], 51: ['дама бубна', 10], 52: ['король бубна', 10], 53: ['туз бубна', 11, 1]
            }

    def __init__(self, index):
        self.index = index


class Deck:
    def __init__(self):
        self.cards = [Card(index) for index in range(2, 54)]


class Player:
    def __init__(self, name, point=0):
        self.name = name
        self.point = point
        self.carts_list = []

    def add_cart(self):
        random_card = shoe_box_card.pop()
        self.carts_list.append(random_card)
        if random_card in (14, 27, 40, 53) and self.point > 21:
            self.point += Card.cards[random_card][2]
        else:
            self.point += Card.cards[random_card][1]

    def __str__(self):
        return f'{self.name}: {", ".join([Card.cards[i][0] for i in self.carts_list])}, {self.point} очков'


def game(player, dealer):
    while True:
        random.shuffle(shoe_box_card)
        print('Старт игры! Диллер раздает по две карты\n')
        player.add_cart()
        player.add_cart()
        dealer.add_cart()
        dealer.add_cart()
        print(player)
        while True:
            try:
                question = int(input('Играем - 1, Останавливаемся - 2:  '))
                if question == 1:
                    player.add_cart()
                    print(player)
                    if player.point > 21:
                        print('\nПеребор!')
                        print(dealer)
                        print(get_winner(player, dealer))
                        break
                elif question == 2:
                    while dealer.point <= 17:
                        print(f'{dealer.name} себе!')
                        dealer.add_cart()
                    print('Вскрываем карты!\n')
                    print(dealer)
                    print(player)
                    print(get_winner(player, dealer))
                    break
            except ValueError:
                print('ошибка ввода!')
        while True:
            try:
                ask = int(input('\nСыграем еще раз? да - 1, нет - 2:  '))
                if ask == 1:
                    player.carts_list, player.point = [], 0
                    dealer.carts_list, dealer.point = [], 0
                    break
                elif ask == 2:
                    return
                else:
                    raise ValueError
            except ValueError:
                print('ошибка ввода!')


def get_winner(player, dealer):
    if 21 > player.point == dealer.point:
        return f'Ничья! У угроков по {player.point} очков'
    elif player.point == 21:
        return f'Победитель {player.name}!'
    elif dealer.point == 21:
        return f'Победитель {dealer.name}!'
    elif 21 > player.point > dealer.point:
        return f'Победитель {player.name}!'
    elif 21 > dealer.point > player.point:
        return f'Победитель {dealer.name}!'
    elif player.point > 21:
        return f'Победитель {dealer.name}!'
    else:
        return f'Победитель {player.name}!'


ivan = Player('Иван')
casino = Player('Диллер')
game(ivan, casino)
