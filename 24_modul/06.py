class Water:
    def __add__(self, other):
        if isinstance(other, Air):
            return Storm()
        elif isinstance(other, Fire):
            return Steam()
        elif isinstance(other, Earth):
            return Dirt()
        elif isinstance(other, Metal):
            return Corrosion()


class Air:
    def __add__(self, other):
        if isinstance(other, Fire):
            return Lightning()
        elif isinstance(other, Earth):
            return Dust()
        elif isinstance(other, Water):
            return Storm()


class Fire:
    def __add__(self, other):
        if isinstance(other, Earth):
            return Lava()
        elif isinstance(other, Water):
            return Steam()
        elif isinstance(other, Air):
            return Lightning()
        elif isinstance(other, Metal):
            return Melting()


class Earth:
    def __add__(self, other):
        if isinstance(other, Water):
            return Dirt()
        elif isinstance(other, Fire):
            return Lava()
        elif isinstance(other, Air):
            return Dust()


class Metal:
    def __add__(self, other):
        if isinstance(other, Water):
            return Corrosion()
        elif isinstance(other, Fire):
            return Melting()


class Storm:
    answer = 'Вода + Воздух = Шторм'


class Steam:
    answer = 'Вода + Огонь = Пар'


class Dirt:
    answer = 'Вода + Земля = Грязь'


class Lightning:
    answer = 'Воздух + Огонь = Молния'


class Lava:
    answer = 'Огонь + Земля = Лава'


class Dust:
    answer = 'Воздух + Земля = Пыль'


class Corrosion:
    answer = 'Металл + Вода = Крейсер "Мацква"'


class Melting:
    answer = 'Металл + Огонь = Война'


water = Water()
air = Air()
fire = Fire()
earth = Earth()
metal = Metal()

while True:
    print('\nСтихии: вода, воздух, огонь, земля, металл')
    application_1 = input('Что мешаем? \n')
    application_2 = input('С чем мешаем? \n')
    if application_1 == 'вода':
        application_1 = water
    elif application_1 == 'воздух':
        application_1 = air
    elif application_1 == 'огонь':
        application_1 = fire
    elif application_1 == 'земля':
        application_1 = earth
    elif application_1 == 'металл':
        application_1 = metal
    if application_2 == 'вода':
        application_2 = water
    elif application_2 == 'воздух':
        application_2 = air
    elif application_2 == 'огонь':
        application_2 = fire
    elif application_2 == 'земля':
        application_2 = earth
    elif application_2 == 'металл':
        application_2 = metal
    try:
        summ = application_1+application_2
        print(summ.answer)
    except BaseException:
        print('None')
