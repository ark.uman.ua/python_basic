import random


class Cell:
    def __init__(self, index, content=' '):
        self.index, self.content = index, content


class Board:
    def __init__(self):
        self.list = [Cell(i) for i in range(1, 10)]

    def chek_cell(self, i):
        if self.list[i].content == ' ':
            return True
        else:
            return False

    def __str__(self):
        for i in range(3):
            print(self.list[i*3].content, '|', self.list[i*3+1].content, '|', self.list[i*3+2].content)
            print('-'*11)


class Player:
    def __init__(self, name, cell_num=''):
        self.name, self.cell_num = name, cell_num


def step_user(board):
    while True:
        try:
            user_cell_num = int(input('Куда ставим Х ? '))
        except ValueError:
            print('Ошибка ввода!')
            continue
        if 1 <= user_cell_num <= 9:
            if board.chek_cell(user_cell_num-1):
                board.list[user_cell_num-1].content = 'X'
                break
            else:
                print('Клетка занята! Попробуйте другую')
        else:
            print('В игре нет такой клетки!')


def step_machine(board):
    free_cells = [i.index for i in board.list if i.content != 'O' and i.content != 'X']
    board.list[random.choice(free_cells)-1].content = 'O'


def check_win(board):
    win_coord = ((0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6), (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6))
    for each in win_coord:
        if board.list[each[0]].content == board.list[each[1]].content == board.list[each[2]].content:
            return board.list[each[0]].content


def play(user, machine, board):
    count = 0
    print('Старт игры!\n')
    board.list[4].content = 'O'
    while True:
        board.__str__()
        step_user(board)
        step_machine(board)
        count += 1
        if count >= 2:
            if check_win(board) == 'X':
                board.__str__()
                print(f'Победитель {user.name}!')
                break
            elif check_win(board) == 'O':
                board.__str__()
                print(f'Победитель {machine.name}!')
                break
        if count == 4:
            print('Ничья!')
            board.__str__()
            break


ivan = Player('Иван')
computer = Player('Компьютер')
my_board = Board()

print('Игра: Крестики-нолики. Игровое поле с номерами клеток\n 1 | 2 | 3 '
      '\n-----------\n 4 | 5 | 6 \n-----------\n 7 | 8 | 9 ')
play(ivan, computer, my_board)

