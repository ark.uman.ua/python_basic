class Unit:
    name, heals = 'Воин', 100

    def __init__(self, index):
        self.index = index

    def attack(self):
        print(f'\nАтаковал {self.name}{self.index}!')

    def down_heals(self):
        self.heals -= 20
        print(f'У {self.name}{self.index} осталось здоровья {self.heals} очков\n')
        if self.heals == 0:
            return True
        return False

    def winner(self):
        print(f'Победил {self.name}{self.index}!')


def main(user_1, user_2):
    while True:
        attack_index = input('Кто бьёт 1 или 2? ')
        if attack_index == '1':
            user_1.attack()
            if user_2.down_heals():
                user_1.winner()
                break
        elif attack_index == '2':
            user_2.attack()
            if user_1.down_heals():
                user_2.winner()
                break
        else:
            print('Ошибка: неизвестная команда\n')


player_1 = Unit(1)
player_2 = Unit(2)
main(player_1, player_2)


