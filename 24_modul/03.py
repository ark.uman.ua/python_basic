import math


class Circle:

    def __init__(self, count, x=0, y=0, radius=1):
        self.count, self.x, self.y, self.radius = count, x, y, radius

    def area(self):
        return f'Круг{self.count}: площадь {round(math.pi * self.radius ** 2, 2)}'

    def perimeter(self):
        return f'Круг{self.count}: периметр {round(2*math.pi*self.radius, 2)}'

    def scale(self, n):
        self.radius *= n
        print(f'Круг{self.count} увеличен в {n} раз')

    def is_intersect(self, other):
        print(f'Круги {self.count} и {other.count}', end=' ')
        if (self.x - other.x) ** 2 + (self.y - other.y) ** 2 >= (self.radius + other.radius) ** 2:
            print('не пересекаются')
        else:
            print('пересекаются')


c_1 = Circle(1)
c_2 = Circle(2, -3, 0, 2)
c_3 = Circle(3, 0, -1)
print(c_1.perimeter())
print(c_1.area())
print(c_2.perimeter())
print(c_2.area())

c_1.is_intersect(c_2)
c_1.scale(2)
c_1.is_intersect(c_2)
c_2.is_intersect(c_3)
c_3.scale(2)
c_1.is_intersect(c_3)

