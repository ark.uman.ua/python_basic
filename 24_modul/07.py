import random


class People:
    def __init__(self, name, house, satiety=50):
        self.name, self.house, self.satiety = name, house, satiety

    def eat(self):
        self.satiety += 20
        self.house.food -= 20
        print(f'{self.name} ест')

    def work(self):
        self.satiety -= 20
        self.house.money += 100
        print(f'{self.name} работет')

    def play(self):
        self.satiety -= 10
        print(f'{self.name} играет')

    def shopping(self):
        self.house.food += 50
        self.house.money -= 50
        print(f'{self.name} купил еды')

    def live_one_day(self):
        action = random.randint(1, 6)
        if self.satiety < 20:
            self.eat()
        elif self.house.food < 10 and self.house.money >= 50:
            self.shopping()
        elif self.house.money < 50:
            self.work()
        if action == 1:
            self.work()
        elif action == 2:
            self.eat()
        else:
            self.play()
        if self.satiety < 0 and self.house.food < 0 or self.house.money < 0:
            return False
        else:
            return True


class House:
    def __init__(self, food=50, money=0):
        self.food, self.money = food, money


def main(period):
    count_day = 1
    corpse = ''
    model_viable = True
    while count_day <= period:
        print(f'\nДень {count_day}.')
        for i_people in family:
            if not i_people.live_one_day():
                count_day = period
                corpse = i_people.name
                model_viable = False
                break
        count_day += 1
    if model_viable:
        print('\nМодель жизнеспособна! Все выжыли, в доме осталось {} денег'.format(home.money))
    else:
        print(f'\n{corpse} умер от голода! Модель не жизнеспособна!')


home = House()
adam = People('Адам', home)
eva = People('Ева', home)
family = [adam, eva]

main(365)

