class Potato:
    status = {0: 'отсутствует', 1: 'росток', 2: 'зеленая', 3: 'зрелая'}

    def __init__(self, index):
        self.index = index
        self.state = 0

    def grow(self):
        if self.state < 3:
            self.state += 1
        self.print_state()

    def is_ripe(self):
        if self.state == 3:
            return True
        return False

    def print_state(self):
        print(f'Картошка {self.index} сейчас {Potato.status[self.state]}')


class BedsPotato:
    def __init__(self, count):
        self.potatoes = [Potato(index) for index in range(1, count+1)]

    def all_grow(self):
        print('Картоха прорастает!')
        for i_potato in self.potatoes:
            i_potato.grow()

    def all_ripe(self):
        if not all([i_potato.is_ripe() for i_potato in self.potatoes]):
            print('Картоха еще не созрела')
        else:
            print('Картошка созрела, пора собирать!\n')


class Gardener:
    def __init__(self, name, result=[]):
        self.name, self.result = name, result

    def carest(self, garden):
        if garden.potatoes[1].state == 1 or garden.potatoes[1].state == 2:
            print('Грядкa очищена от сорняков и обработана от вредителей инсектицидом')
        else:
            print('Грядка не нуждается в уходе')

    def harvesting(self, garden):
        self.result.append(1)
        for i_potato in my_garden.potatoes:
            i_potato.state = 0
        print('{} собрал урожай {} раз!'.format(self.name, sum(self.result)))

    def choice_action(self, garden):
        answer = int(input('\n{} готов ухаживать за грядками! Отправить?\n1 - да, 2 - нет\n'.format(self.name)))
        if answer == 1:
            self.carest(garden), garden.all_grow(), garden.all_ripe()
        elif answer == 2:
            print('А напрастно! Ему не мешало бы поработать!')
        if all([i_potato.is_ripe() for i_potato in garden.potatoes]):
            answer = int(input('Собрать картошку? \n1 - да, 2 - нет\n'))
            if answer == 1:
                self.harvesting(garden)
            elif answer == 2:
                garden.all_ripe()


def main(worker, garden):
    while True:
        try:
            worker.choice_action(garden)
        except ValueError:
            print('Непонятная команда!')


my_garden = BedsPotato(5)
fill = Gardener('Фил')
main(fill, my_garden)
