# Задача 8. Контакты 3
# Мы уже помогали Степану с реализацией телефонной книги (контактов) на телефоне, однако внезапно оказалось, что книге
# не хватает ещё одной очень полезной функции: поиска! Напишите программу, которая бесконечно запрашивает у
# пользователя действие, которое он хочет совершить: добавить контакт или найти человека в списке контактов по фамилии.
# Действие «Добавить контакт»: программа запрашивает имя и фамилию контакта, затем номер телефона, добавляет их в
# словарь и выводит на экран текущий словарь контактов. Если этот человек уже есть в словаре, то выведите
# соответствующее сообщение. Действие «Поиск человека по фамилии»: программа запрашивает фамилию и выводит все контакты
# с такой фамилией и их номера телефонов. Поиск не должен зависеть от регистра символов.

def add_contact(my_dict):
    surname, name, number = map(str, input('\nВведите фамилию контакта, имя контакта и номер: ').split())
    for i_surname in my_dict.keys():
        if surname in i_surname[0]:
            print('Такой контакт уже существует')
            break
    else:
        my_dict[surname, name] = number
        print('Контакт добавлен. Вывожу новый список контактов:')
    for name, number in my_dict.items():
        print(*name, ':', number)
    return my_dict

def search_contact(my_dict):
    surname = input('\nДавай фамилию, сейчас поищем: ').lower()
    for name, number in my_dict.items():
        if surname in name[0].lower() or (surname + 'a') in name[0].lower() or (surname[:-1]) in name[0].lower():
            print(*name, ':', number)
            break


def search_contact(user_dict):
    user_sur = input('Введите фамилию: ')
    check = False
    for i_key, i_val in user_dict.items():
        if user_sur.lower() in i_key[1].lower():
            check = True
            print('{surname} {name} {age}'.format(surname=i_key[0], name=i_key[1], age=i_val))
    if not check:
        print('Человека с такой фамилией в тел.книге нет')


def add_new_contact(cont_dict):
    user_contact = tuple((input('Имя и фамилия: ')).split(' '))
    if user_contact in cont_dict:
        print('Такой человек уже есть в книге')
    else:
        num = int(input(' номер: '))
        cont_dict[user_contact] = num
        print(cont_dict)
    return cont_dict


data = {
    ('Аня', 'Синюк'): 45658,
    ('Ігорь', 'Внуков'): 4521676,
    ('Аня', 'Камазова'): 45892132,
    ('Кент', 'Вчерашний'): 65812123,
    ('Сто', 'Умань'): 78444564,
}

while True:
    action = input('Действие: Добавить контакт - 1 / Поиск человека по фамилии - 2): ')
    if action == '1':
        data = add_new_contact(data)
    elif action == '2':
        search_contact(data)
    else:
        print('Ошибка: неверное действие')


