# Задача 7. Функция сортировки
# Напишите функцию, которая сортирует кортеж, состоящий из целых чисел, по возрастанию и возвращает его.
# Если хотя бы один элемент не является целым числом, то функция возвращает исходный кортеж.

import random


def sort_int_tpl(user_tuple):
    for i_val in user_tuple:
        if not isinstance(i_val, int):
            return user_tuple
    return sorted(tuple(list(user_tuple)))
    
rdm_tuple_2 = tuple(round(random.uniform(1, 10), 2) for _ in range(0, 10))
correct_tuple = tuple([random.randint(1, 10) for _ in range(15)])
not_correct_tuple = (1, 4, 1.2, 40, 15)

print(sort_int_tpl(not_correct_tuple))
print(sort_int_tpl(correct_tuple))
