# Задача 6. По парам
# Есть список из 10 случайных чисел. Напишите программу, которая делит эти числа на пары кортежей внутри списка,
# и выведите результат на экран.
# Дополнительно: решите задачу несколькими способами.
# Пример:
# Оригинальный список: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# Новый список: [(0, 1), (2, 3), (4, 5), (6, 7), (8, 9)]

def way_1(user_list):
    new_list = []
    for i in range(0, 10, 2):
        temp_tuple = (user_list[i], user_list[i+1])
        new_list.append(temp_tuple)
    return new_list


def way_2(user_list):
    new_list = []
    for k, i in enumerate(user_list):
        if k % 2 == 0:
            temp_tuple = i, user_list[k + 1]
            new_list.insert(k, temp_tuple)
    return new_list


origin_list = [0, 11, 12, 13, 14, 15, 16, 17, 18, 19]

print(way_1(origin_list))
print(way_2(origin_list))

