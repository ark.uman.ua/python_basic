# Задача 1. Ревью кода
# Ваня работает middle-разработчиком на Python в IT-компании. Один кандидат на junior-разработчика прислал ему код
# тестового задания. Задание состояло в следующем: есть словарь из трёх студентов. Необходимо:
# 1. Вывести на экран список пар «ID студента — возраст».
# 2. Написать функцию, которая принимает в качестве аргумента словарь и возвращает два значения: полный список интересов
# всех студентов и общую длину всех фамилий студентов.
# 3. Далее в основном коде эта функция вызывается, и значения присваиваются отдельным переменным, которые после выводятся
# на экран. (Т.е. нужно распаковать все возвращаемые значения в отдельные переменные.)
# Ваня — очень придирчивый программист, и после просмотра кода он понял, что этого кандидата на работу не возьмёт,
# даже несмотря на то, что он выдаёт верный результат. Вот сам код кандидата:
# Перепишите этот код так, чтобы он был максимально pythonic и Ваня мало к чему мог придраться (ну только если очень
# захочется). Убедитесь в том, что программа работает всё так же верно. Различные проверки на существование записей в
# словаре не обязательны, но приветствуются :)

def interest(user_dict):
    int_list = list()
    count_let_surname = 0
    for i_val in user_dict.values():
        for i_int in i_val['interests']:
            if i_int not in int_list:
                int_list.append(i_int)
        count_let_surname += len(i_val['surname'])
    return int_list, count_let_surname


students = {
    1: {
        'name': 'Bob',
        'surname': 'Vazovski',
        'age': 23,
        'interests': ['biology', 'swimming']
    },
    2: {
        'name': 'Rob',
        'surname': 'Stepanov',
        'age': 24,
        'interests': ['math', 'computer games', 'running']
    },
    3: {
        'name': 'Alexander',
        'surname': 'Krug',
        'age': 22,
        'interests': ['languages', 'health food']
    }
}

pairs = {id: students[id]['age'] for id in students}
print('\nID студента — возраст')
for i_id, i_age in pairs.items():
    print('{id} - {age}'.format(id=i_id, age=i_age))

my_lst, letter = interest(students)

print('\nИнтересы: {0}\nОбщая длина фамилий: {1}'.format(my_lst, letter))

