# Задача 2. Универсальная программа 2
# Спустя некоторое время заказчик попросил нас немного изменить скрипт для своей криптографии: теперь индексы элементов
# должны быть простыми числами. Напишите функцию, которая возвращает список из элементов итерируемого объекта
# (кортежа, строки, списка, словаря), у которых индекс — это простое число. Для проверки на простое число напишите
# отдельную функцию is_prime.
# Дополнительно: сделайте так, чтобы основная функция состояла только из оператора return и при этом также возвращала
# список.

def typeof(user_text):
    if isinstance(user_text, dict):
        return [v for k, v in user_text.items() if is_prime(k)]
    elif isinstance(user_text, list) or isinstance(user_text, tuple) or isinstance(user_text, str):
        return [i_val for i_ind, i_val in enumerate(user_text) if is_prime(i_ind)]
    else:
        print("type is unknown")


def is_prime(num):
    if 0 <= num <= 1:
        return False
    for i in range(2, num):
        if num % i == 0:
            return False
    return True


def prime_index_list():
    return typeof(my_list), typeof(my_dict), typeof(my_string), typeof(my_tuple)


my_list = [100, 200, 300, 'буква', 0, 2, 'а']
my_dict = {0: 10, 1: 20, 2: 300, 3: 'd', 4: 5.1, 5: 'f', 6: 0.787, 7: 81, 8: 9900, 9: 10}
my_string = 'О Дивный Новый мир!'
my_tuple = (1, 2, 93, 4, 75, 6, 57, 78, 9, 10)

print('{0}\n{1}\n{2}\n{3}'.format(prime_index_list()[0], prime_index_list()[1],
                                  prime_index_list()[2], prime_index_list()[3]))

