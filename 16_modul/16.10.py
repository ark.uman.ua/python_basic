#Задача 10. Симметричная последовательность

numbers_list = []

number = int(input('Кол-во чисел: '))
for _ in range(number):
    elem_list = int(input('Число: '))
    numbers_list.append(elem_list)

for i in range(number-1, -1, -1):
    if numbers_list[i] != numbers_list[i-1]:
        count = i
        break

print('\nПоследовательность: ', end='')
for i_num in numbers_list:
    print(i_num, end=' ')

print('\nНужно приписать чисел: ', count)

print('Сами числа: ', end='')
for i in range(count-1, -1, -1):
    print(numbers_list[i], end=' ')