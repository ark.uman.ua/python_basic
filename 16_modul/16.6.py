#Задача 6. Уникальные элементы

first_list = []
second_list = []

print('Формируем первый список')
for _ in range(3):
    element_first_list = int(input('Введите число: '))
    first_list.append(element_first_list)

print('Формируем второй список')
for _ in range(7):
    element_second_list = int(input('Введите число: '))
    second_list.append(element_second_list)

print('\nПервый список: ', first_list)
print('Второй список:', second_list)

first_list.extend(second_list)

for i_elem in first_list:
     while first_list.count(i_elem) > 1:
         first_list.remove(i_elem)

print('\nНовый первый список с уникальными элементами: ', first_list)

