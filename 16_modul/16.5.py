#Задача 5. Песни

violator_songs = [['World in My Eyes', 4.86], ['Sweetest Perfection', 4.43],
                  ['Personal Jesus', 4.56], ['Halo', 4.9],
                  ['Waiting for the Night', 6.07], ['Enjoy the Silence', 4.20],
                  ['Policy of Truth', 4.76], ['Blue Dress', 4.29], ['Clean', 5.83]]
like_songs = []

num_songs = int(input('Сколько песень выбрать?  '))
time_songs = 0

for i_num in range(num_songs):
    print('Название', i_num+1, 'песни:', end=' ')
    name_song = input()
    for i_song in violator_songs:
        if name_song in i_song:
            like_songs.append(i_song)
            time_songs += like_songs[like_songs.index(i_song)][1]

print('\nСписок избраных песен: ', like_songs, '\nОбщее время звучания песен: ', time_songs, 'минут')
