#Задача 7. Ролики

skates = []
people_size = []
count_people = 0

skates_number = int(input('К-во коньков: '))
for i_elem in range(skates_number):
    print('Размер', i_elem+1, 'пары:', end=' ')
    skates.append(int(input('')))

people_number = int(input('\nК-во людей: '))
for i_elem in range(people_number):
    print('Размер ноги', i_elem+1, 'человека:', end=' ')
    people_size.append(int(input('')))

for i_size in people_size:
    for i_skates in skates:
        if i_skates >= i_size:
            count_people += 1
            skates.remove(i_skates)
            break

print('\nНаибольшее кол-во людей, которые могут взять ролики: ', count_people)
print('\nОставшиеся размеры: ', skates)
