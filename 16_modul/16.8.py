#Задача 8. Считалка

num_player = int(input('К-во человек: '))
number_out = int(input('Какое число в считалке? '))

players = list(range(1, num_player+1))

print('Значит, выбывает каждый', number_out, 'человек')

start = 0
while len(players) > 1:
    print('\nТекущий круг людей: ', players)
    print('Начало счёта с номера', players[start])
    result = (start + number_out - 1) % len(players)
    start = (0 if players[result] == players[-1] else result)
    print('Выбывает человек под номером: ', players.pop(result))

print('\nОстался человек под номером: ', players[0])
