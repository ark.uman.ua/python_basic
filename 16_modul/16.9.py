#Задача 9. Друзья

credit_list = []
number_friend = int(input('К-во друзей: '))
number_credit = int(input('К-во долговых расписок: '))

for _ in range(number_friend):
    credit_list.append(0)

for i_num in range(1, number_credit + 1):
    print('\n', i_num, 'расписка')
    credit_num = int(input('Кому: '))
    debit_num = int(input('От кого: '))
    summa = int(input('Сколько: '))
    credit_list[credit_num-1] -= summa
    credit_list[debit_num-1] += summa

print('\nБаланс друзей:')
for i_count in range(number_friend):
    print(i_count + 1, ':', credit_list[i_count])