#Задача 4. Вечеринка

guests = ['Петя', 'Ваня', 'Саша', 'Лиза', 'Катя']

while True:
    print('\nСейчас на вечеринке', len(guests), 'человек: ', guests)
    action = input('Гость пришел или ушел? ')
    if action == 'Пора спать':
        print('\nВечеринка закончилась, все легли спать.')
        break
    else:
        action_quest = input('Имя гостя: ')
        if action == 'пришел':
            if len(guests) < 6:
                print('Привет, ', action_quest+'!')
                guests.append(action_quest)
            else:
                print('Прости, ', action_quest+', но мест нет (')
        if action == 'ушел':
            print('Пока, ', action_quest+'.')
            guests.remove(action_quest)