#Задача 2. Шеренга

first_class = list(range(160, 177, 2))
second_class = list(range(162, 181, 3))
single_class = []

print(first_class)
print(second_class)

single_class.extend(first_class)
single_class.extend(second_class)

for i_elem in range(len(single_class)):
    for i_curr in range(i_elem, len(single_class)):
        if single_class[i_curr] < single_class[i_elem]:
            single_class[i_curr], single_class[i_elem] = single_class[i_elem], single_class[i_curr]

print('\n', single_class)