# Задача 3. Детали

shop = [['каретка', 1200], ['шатун', 1000], ['седло', 300], ['педаль', 100], ['седло', 1500], ['рама', 12000],
        ['обод', 2000], ['шатун', 200], ['седло', 2700]]

name_detail = input('Название детали: ')
count_detail = 0
summ = 0

for i_shop in range(len(shop)):
    if shop[i_shop][0] == name_detail:
        summ += shop[i_shop][1]
        count_detail += 1

print('Кол-во деталей: ', count_detail)
print('Общая стоимость: ', summ)
