#Задача 1. Страшный код

main = [1, 5, 3]
first_side_list = [1, 5, 1, 5]
second_side_list = [1, 3, 1, 5, 3, 3]

main.extend(first_side_list)

print('Кол-во цифр "5" при первом объединении: ', main.count(5))

for i_main in main:
    if i_main == 5:
        main.remove(5)

main.extend(second_side_list)

print('Кол-во цифр "3" при втором объединении: ', main.count(3))
print('Итоговый список: ', main)