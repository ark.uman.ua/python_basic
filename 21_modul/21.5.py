# Задача 5. Ускоряем работу функции
# У нас есть функция, которая делает определённые действия с входными данными:
# Берёт факториал от числа. Результат делит на куб входного числа. И получившиеся число возводит в 10-ю степень.
# def calculating_math_func(data):
#     result = 1
#     for index in range(1, data + 1):
#         result *= index
#     result /= data ** 3
#     result = result ** 10
#     return result
# Однако каждый раз нам приходится делать сложные вычисления, хотя входные и выходные данные одни и те же. И тут наши
# знания тонкостей Python должны нам помочь. Оптимизируйте функцию так, чтобы высчитывать факториал для одного и того
# же числа только один раз. При выполнении этого задания нельзя использовать функцию factorial
# Подсказка: вспомните, что происходит с изменяемыми данными, если их выставить по умолчанию в параметрах функции.

def calculating_math_func(data, factorials={}):
    if isinstance(data, int):
        if data in factorials:
            print(f'значение из словаря для {data}')
            result = factorials[data]
        else:
            result = 1
            for index in range(1, data+1):
                result *= index
            result /= data ** 3
            result **= 10
            factorials[data] = result
        # print(f'Список результатов: {factorials}')   # для перегляду результатів попередніх підрахунків
        print(result)
    elif isinstance(data, list) or isinstance(data, tuple) or isinstance(data, set):
        for i_num in data:
            calculating_math_func(i_num)
    else:
        print('Ошибка ввода')


t1 = 1
t2 = 5, 6, 7.1, 7
t3 = [2, 'f', 9], 1
t4 = (5, 6, 7, 10)

calculating_math_func(t1)
print()
calculating_math_func(t2)
print()
calculating_math_func(t3)
print()
calculating_math_func(t4)


