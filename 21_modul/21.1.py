# Задача 1. Challenge 2
# Вдохновившись мотивацией Антона, ваш друг тоже решил поставить перед собой задачу, но не сильно связанную с
# математикой, а именно: написать функцию, которая выводит все числа от 1 до num без использования циклов.
# Помогите другу реализовать такую функцию.

def print_num(num_start, num_stop):
    if num_start == num_stop:
        return num_stop
    print(num_start)
    return print_num(num_start+1, num_stop)


user_num = int(input('Введите число: '))

print(print_num(1, user_num))
