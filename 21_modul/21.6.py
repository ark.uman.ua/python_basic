# Задача 6. Глубокое копирование
# Вы сделали для заказчика структуру сайта по продаже телефонов:
# Заказчик рассказал своим коллегам на рынке, и они тоже захотели такой сайт, только для своих товаров. Вы посчитали,
# что это лёгкая задача, и быстро принялись за работу. Напишите программу, которая запрашивает у клиента, сколько будет
# сайтов, а затем запрашивает название продукта и после каждого запроса выводит на экран активные сайты.
# Условия: структуру сайта нужно описать один раз, копипасту никто не любит.
# Подсказка: используйте рекурсию.
# Пример:
# Сколько сайтов: 2
# Введите название продукта для нового сайта: iPhone
# Сайт для iPhone:
# site = {
#     'html': {
#         'head': {
#             'title': 'Куплю/продам iPhone недорого'
#         },
#         'body': {
#             'h2': 'У нас самая низкая цена на iPhone',
#             'div': 'Купить',
#             'p': 'Продать'
#         }
#     }
# }
# Введите название продукта для нового сайта: Samsung
# Сайт для iPhone:
# site = {
#     'html': {
#         'head': {
#             'title': 'Куплю/продам iPhone недорого'
#         },
#         'body': {
#             'h2': 'У нас самая низкая цена на iPhone',
#             'div': 'Купить',
#             'p': ‘Продать'
#         }
#     }
# }
# Сайт для Samsung:
# site = {
#     'html': {
#         'head': {
#             'title': 'Куплю/продам Samsung недорого'
#         },
#         'body': {
#             'h2': 'У нас самая низкая цена на Samsung,
#             'div': 'Купить',
#             'p': ‘Продать'
#         }
#     }
# }
import json
import copy


def charge_site(struct, prod, name1='телефон', name2='iPhone'):
    for i_k, i_val in struct.items():
        if isinstance(i_val, str):
            if name1 or name2 in i_val:
                i_val = i_val.replace(name1, prod)
                i_val = i_val.replace(name2, prod)
                struct[i_k] = i_val
        if isinstance(i_val, dict):
            charge_site(i_val, prod, name1='телефон', name2='iPhone')


site = {
    'html': {
        'head': {
            'title': 'Куплю/продам телефон недорого'
        },
        'body': {
            'h2': 'У нас самая низкая цена на iPhone',
            'div': 'Купить',
            'p': 'Продать'
        }
    }
}

new_sites = dict()

count_site = int(input('Сколько сайтов? '))
for i in range(count_site):
    product = input('Введите название продукта для нового сайта: ')
    temp_site = copy.deepcopy(site)
    charge_site(temp_site, product)
    new_sites[f'Сайт для {product}:'] = copy.deepcopy(temp_site)
    for i_key in new_sites:
        print(i_key)
        print('site =', json.dumps(obj=new_sites[i_key], indent=4, ensure_ascii=False))
