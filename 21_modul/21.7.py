# Задача 7. Продвинутая функция sum.
# Как мы знаем, в Python есть полезная функция sum, которая умеет находить сумму
# элементов списков. Но иногда базовых возможностей функций не хватает для работы и приходится их усовершенствовать.
# Напишите свою функцию sum, которая должна быть более гибкой, чем стандартная функция sum. Вот что она должна уметь
# делать: Складывать числа из списка списков. Складывать из набора параметров.
# Примеры вызовов функции:
#
# sum([[1, 2, [3]], [1], 3])
# Ответ: 10
# sum(1, 2, 3, 4, 5)
# Ответ: 15


def my_sum(*args):
    def unpack_list(in_list):
        result = []
        for i_elem in in_list:
            if isinstance(i_elem, int) or isinstance(i_elem, float):
                result.append(i_elem)
            else:
                result.extend(unpack_list(i_elem))
        return result
    return sum(unpack_list(args))


print(my_sum(1, 2, 3, 4, 5))

print(my_sum(0.1, 0.22, 10))

print(my_sum(10, 2, [1.5], 4, 5))

print(my_sum([[1, 2, [3]], [1], 3]))

print(my_sum([[1, (2,), [3, 10]], [1], 3]))







