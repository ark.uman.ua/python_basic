# Задача 8. Список списков 2
# Мы уже работали с многомерными списками и решали задачи, где с помощью list comprehensions «выпрямляли» его в один.
# Однако такой фокус не пройдёт, если у элементов разные уровни вложенности и этих списков неограниченное количество.
# Дан вот такой список:
# nice_list = [1, 2, [3, 4], [[5, 6, 7], [8, 9, 10]], [[11, 12, 13], [14, 15], [16, 17, 18]]]
# Напишите рекурсивную функцию, которая раскрывает все вложенные списки, то есть оставляет только внешний список.
# Ответ: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
# Подсказка: можно возвращать списки и срезы списков.

def unpack_list(lst):
    result = []
    for i_elem in lst:
        if not isinstance(i_elem, list):
            result.append(i_elem)
        else:
            result.extend(unpack_list(i_elem))
    return result


def unpack_list_2(lst):
    if not lst:
        return lst
    if isinstance(lst[0], list):
        return unpack_list_2(lst[0]) + unpack_list_2(lst[1:])
    return lst[:1] + unpack_list_2(lst[1:])


nice_list = [1, 2, [3, 4], [[5, 6, 7], [8, 9, 10]], [[11, 12, [13]], [14, 15], [16, 17, 18]]]

print(unpack_list(nice_list))
print(unpack_list_2(nice_list))
