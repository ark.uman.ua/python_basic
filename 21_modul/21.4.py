# Задача 4. Поиск элемента 2
# Пользователь вводит искомый ключ. Если он хочет, то может ввести максимальную глубину — уровень, до которого будет
# просматриваться структура. Напишите функцию, которая находит заданный пользователем ключ в словаре и выдаёт значение
# этого ключа на экран. По умолчанию уровень не задан. В качестве примера можно использовать такой словарь:

site = {
    'html': {
        'head': {
            'title': 'Мой сайт'
        },
        'body': {
            'h2': 'Здесь будет мой заголовок',
            'div': 'Тут, наверное, какой-то блок',
            'p': 'А вот здесь новый абзац'
        }
    }
}


def search(key, struct, depth=None):
    if depth == 0:
        return None
    if not depth:
        depth = 999
    if key in struct:
        return struct[key]
    depth -= 1
    for sub_str in struct.values():
        if isinstance(sub_str, dict):
            result = search(key, sub_str, depth)
            if result:
                break
    else:
        result = None
    return result


user_key = input('Ключ: ')

user_depth = input('Глубина поиска в уровнях (пропустить - Enter): ')

if user_depth == '':
    values = search(user_key, site)
else:
    values = search(user_key, site, depth=int(user_depth))

if values:
    print(values)
else:
    print('Такого ключа в базе нет')
