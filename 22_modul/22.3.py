# Задача 3. Дзен Пайтона 2
# Напишите программу, которая определяет, сколько букв (латинского алфавита), слов и строк в файле zen.txt
# (который содержит всё тот же Дзен Пайтона). Выведите три найденных числа на экран.
#
# Дополнительно: выведите на экран букву, которая встречается в тексте наименьшее количество раз.

data = open('zen.txt', 'r')

letters = dict()
for i_line in data:
    for i_elem in i_line:
        if i_elem.isalpha():
            i_elem = i_elem.lower()
            if i_elem in letters:
                letters[i_elem] += 1
            else:
                letters[i_elem] = 1
min_count = min(letters.values())
min_count_letter = ''
for i_key, i_val in letters.items():
    if i_val == min_count:
        min_count_letter = i_key
        break
count_letters = sum(letters.values())
data.seek(0)
count_str = len(list(1 for i_line in data))
data.seek(0)
count_words = sum([len(i_line.split()) for i_line in data])
data.seek(0)
numbers = list('one' for i_line in data for i_word in i_line.split() if 'one' in i_word)
data.close()

print(f'Букв: {count_letters} \nСтрок: {count_str} \nСлов: {count_words} \nЧисла: {numbers}'
      f'\nБуква, которая встречается в тексте наименьшее количество раз - {min_count_letter}')



