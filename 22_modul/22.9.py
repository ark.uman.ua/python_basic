# Задача 9. Война и мир (необязательная)
# Мало кто не знает про знаменитый роман Л. Н. Толстого «Война и мир». Это довольно объёмное произведение лежит в
# архиве voina-i-mir.zip. Напишите программу, которая подсчитает статистику по буквам (не только русского алфавита) в
# этом романе и выведет результат на экран (или в файл). Результат должен быть отсортирован по частоте встречаемости
# букв (по возрастанию или убыванию). Регистр символов имеет значение.
# Постарайтесь написать программу так, чтобы для её работы не требовалась распаковка архива «вручную».

# Важливо! щоб код працював треба кинути в корінь диску на якому активний проект архів voina-i-mir.zip. Довелось його
# самостійно створити із чотирьох томів з інету. Архів містить один файл txt із повним тектом роману.

import os
import zipfile


import os
import zipfile
from operator import itemgetter


path = os.path.abspath(os.path.join('', 'voina-i-mir.zip'))
with zipfile.ZipFile(path, 'r') as zip_file:
    zip_file.extractall('')

file = open('voina-i-mir.txt', 'r', encoding='utf-8')
data = file.read()

letters = dict()
for i_line in data:
    for i_elem in i_line:
        if i_elem.isalpha():
            i_elem = i_elem.lower()
            if i_elem in letters:
                letters[i_elem] += 1
            else:
                letters[i_elem] = 1
file.close()
result_list = [(k, v) for k, v in letters.items()]
result_list.sort(key=itemgetter(1), reverse=True)

file = open('letters_voina-i-mir.txt', 'w', encoding='utf-8')
for i_elem in result_list:
    file.write(f'{i_elem[0]} : {i_elem[1]}\n')
    print(i_elem)
file.close()

