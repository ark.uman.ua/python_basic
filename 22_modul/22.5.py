# Задача 5. Сохранение
# Мы продолжаем работать над усовершенствованием своего текстового редактора. Конечно же, при работе с редактором
# пользователь, скорее всего, захочет сохранить то, что он написал, в отдельный файл. Ну или перезаписать тот, в котором
# он продолжил работать. Пользователь вводит строку text. Реализуйте функцию, которая запрашивает у пользователя, куда
# он хочет сохранить эту строку: вводится последовательность папок и имя файла (расширение .txt). Затем в этот файл
# сохраняется значение переменной text. Если этот файл уже существует, то нужно спросить у пользователя, действительно
# ли он хочет перезаписать его. Обеспечьте контроль ввода: указанный из папок путь должен существовать на диске.
# Пример 1:
# Введите строку: programm test
# Куда хотите сохранить документ? Введите последовательность папок (через пробел):
# Users Roman PycharmProjects Skillbox Module22
# Введите имя файла: my_document
# Файл успешно сохранён!
# Содержимое файла:
# programm test
# Пример 2:
# Введите строку: testiruyem
# Куда хотите сохранить документ? Введите последовательность папок (через пробел):
# Users Roman PycharmProjects Skillbox Module22
# Введите имя файла: my_document
# Вы действительно хотите перезаписать файл? да
# Файл успешно перезаписан!
# Содержимое файла:
# testiruyem

import os


def check_input():
    while True:
        print('Куда хотите сохранить документ? Введите последовательность папок (через пробел):', end=' ')
        user_list = input().split()
        user_path = os.path.join(*user_list)
        if os.path.exists(user_path):
            return user_path
        else:
            print('Ошибка: указаный путь не существует, попробуйте ещё раз')


def save_file(some_path):
    user_file_name = input('Введите имя файла: ')+'.txt'
    path = os.path.join(some_path, user_file_name)
    if os.path.exists(path):
        print('Файл с таким именем уже существует.\nВы действительно хотите перезаписать файл?', end=' ')
        answer = input()
        if answer == 'да':
            write_file(path, user_string)
            print('Файл успешно перезаписан!')
    else:
        write_file(path, user_string)
        print('Файл успешно сохранен.')
    return path


def write_file(path, string):
    file = open(path, 'w')
    file.write(string)
    file.close()


def print_file(path):
    print('Содержимое файла:')
    file = open(path, 'r')
    data = file.read()
    print(data)
    file.close()


user_string = input('Введите строку: ')
print_file(save_file(check_input()))
