# Задача 1. Сумма чисел 2
# Во входном файле numbers.txt записано N целых чисел, которые могут быть разделены пробелами и концами строк.
# Напишите программу, которая выводит сумму чисел в выходной файл answer.txt.
# Пример:
# Содержимое файла numbers.txt
#      2
#
# 2
#   2
#          2
# Содержимое файла answer.txt
# 8


def create_new_file():
    new_file = open('numbers.txt', 'w')
    some_data = ' 0     2\n 2\n    2      5\n  4\n      2          1'
    new_file.write(some_data)
    new_file.close()


def print_file(file_name):
    file = open(file_name, 'r')
    print(f'Содержимое файла {file_name}')
    for i_line in file:
        print(i_line)
    file.close()


create_new_file()

result_file = open('answer.txt', 'w')
data = open('numbers.txt', 'r')
results = (int(i_elem) for i_line in data for i_elem in i_line if not (i_elem == ' ' or i_elem == '\n'))
result_file.write(str(sum(results)))
data.close()
result_file.close()

print_file('numbers.txt')
print_file('answer.txt')



