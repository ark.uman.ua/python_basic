# Задача 2. Дзен Пайтона
# В файле zen.txt хранится так называемый Дзен Пайтона — текст философии прог-рования на языке Python. Выглядит он так:
# Beautiful is better than ugly.
# Explicit is better than implicit.
# Simple is better than complex.
# Complex is better than complicated.
# Flat is better than nested.
# Sparse is better than dense.
# Readability counts.
# Special cases aren't special enough to break the rules.
# Although practicality beats purity.
# Errors should never pass silently.
# Unless explicitly silenced.
# In the face of ambiguity, refuse the temptation to guess.
# There should be one-- and preferably only one --obvious way to do it.
# Although that way may not be obvious at first unless you're Dutch.
# Now is better than never.
# Although never is often better than *right* now.
# If the implementation is hard to explain, it's a bad idea.
# If the implementation is easy to explain, it may be a good idea.
# Namespaces are one honking great idea -- let's do more of those!
#
# Напишите программу, которая выводит на экран все строки этого файла в обратном порядке.
# Кстати, попробуйте открыть консоль Python и ввести команду “import this”.
#
# Результат работы программы:
# Namespaces are one honking great idea -- let's do more of those!
# If the implementation is easy to explain, it may be a good idea.
# If the implementation is hard to explain, it's a bad idea.
# Although never is often better than *right* now.


file = open('zen.txt', 'w+')
text = "Beautiful is better than ugly.\n"\
       "Explicit is better than implicit.\n"\
       "Simple is better than complex.\n"\
       "Complex is better than complicated.\n"\
       "Flat is better than nested.\n"\
       "Sparse is better than dense.\n"\
       "Readability counts.\n"\
       "Special cases aren't special enough to break the rules.\n"\
       "Although practicality beats purity.\n"\
       "Errors should never pass silently.\n"\
       "Unless explicitly silenced.\n"\
       "In the face of ambiguity, refuse the temptation to guess.\n"\
       "There should be one-- and preferably only one --obvious way to do it.\n"\
       "Although that way may not be obvious at first unless you're Dutch.\n"\
       "Now is better than never.\n"\
       "Although never is often better than *right* now.\n"\
       "If the implementation is hard to explain, it's a bad idea.\n"\
       "If the implementation is easy to explain, it may be a good idea.\n"\
       "Namespaces are one honking great idea -- let's do more of those!\n"

file.write(text)
file.seek(0)

for i_line in file.readlines()[::-1]:
    print(i_line, end='')
file.close()

