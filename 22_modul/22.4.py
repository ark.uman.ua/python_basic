# Задача 4. Файлы и папки
# Напишите программу, которая получает на вход путь до каталога (это может быть и просто корень диска) и выводит общее
# количество файлов и подкаталогов в нём. Также выведите на экран размер каталога в килобайтах (1 килобайт = 1024 байт).
# Важный момент: чтобы посчитать, сколько весит каталог, нужно найти сумму размеров всех вложенных в него файлов.
# Результат работы программы на примере python_basic\Module14:
# Размер каталога (в Кб): 8.373046875
# Количество подкаталогов: 7
# Количество файлов: 15
import os


def count_object(cur_path):
    for i_el in os.listdir(cur_path):
        path = os.path.join(cur_path, i_el)
        if os.path.isdir(path):
            result_lst[0] += 1
            result = count_object(path)
            if result:
                break
        elif os.path.isfile(path):
            result_lst[1] += 1
            result_lst[2] += os.path.getsize(path) / 1024
    else:
        result = None
    return result


result_lst = [0, 0, 0]

user_path = input('путь для поиска: ')
file_path = count_object(user_path)

print(f'Размер каталога (в Кб): {result_lst[2]}\nКоличество подкаталогов: {result_lst[0]}'
      f'\nКоличество файлов: {result_lst[1]}\n')
