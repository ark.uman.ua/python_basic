# Задача 8. Частотный анализ
# Есть файл text.txt, который содержит текст. Напишите программу, которая выполняет частотный анализ, определяя долю
# каждой буквы английского алфавита в общем количестве англ. букв в тексте, и выводит результат в файл analysis.txt.
# Символы, не являющиеся буквами английского алфавита, учитывать не нужно.
# В файл analysis.txt выводится доля каждой буквы, встречающейся в тексте, с тремя знаками в дробной части. Буквы должны
# быть отсортированы по убыванию их доли. Буквы с равной долей должны следовать в алфавитном порядке.
# Пример:
# Содержимое файла text.txt:
# Mama myla ramu.
# Содержимое файла analysis.txt:
# a 0.333
# m 0.333
# l 0.083
# r 0.083
# u 0.083
# y 0.083

from operator import itemgetter


def print_file(name_file, file_object):
    data = file_object.read()
    print(f'Содержимое файла {name_file}:')
    print(data)


def analis(text):
    result_text = ''
    statistics = list()
    count_letters = dict()
    for i_sym in text.lower():
        if i_sym.isalpha():
            if i_sym in count_letters:
                count_letters[i_sym] += 1
            else:
                count_letters[i_sym] = 1
    statistics = [(i_key, round(i_val/sum(count_letters.values()), 3))
                  for i_key, i_val in count_letters.items()]
    statistics.sort(key=itemgetter(0))
    statistics.sort(key=itemgetter(1), reverse=True)
    for i_elem in statistics:
        result_text += f'{i_elem[0]} {i_elem[1]}\n'
    return result_text


file_1 = open('text.txt', 'w+')
file_1.write('Mama myla ramu.')
file_1.seek(0)
print_file('text.txt', file_1)
file_1.seek(0)
file_2 = open('analysis.txt', 'w+')
file_2.write(analis(file_1.read()))
file_2.seek(0)
print_file('analysis.txt', file_2)
file_1.close()
file_2.close()

