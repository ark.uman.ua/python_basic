# Задача 6. Паранойя
# Артуру постоянно кажется, что за ним следят и все хотят своровать «крайне важную информацию» с его компьютера,
# включая переписку с людьми. Поэтому он эти переписки шифрует. И делает это с помощью шифра Цезаря (чем веселит
# агента службы безопасности). Напишите программу, которая шифрует содержимое текстового файла text.txt шифром Цезаря,
# при этом символы первой строки файла должны циклически сдвигаться на один, второй строки — на два, третьей строки — на
# три и так далее. Результат выведите в файл cipher_text.txt.
# Пример:
# Содержимое файла text.txt:
# Hello
# Hello
# Hello
# Hello
# Содержимое файла cipher_text.txt:
# Ifmmp
# Jgnnq
# Khoor
# Lipps


def code_text(text_lst):
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    result = ''
    step = 1
    for i_line in text_lst:
        code_alphabet = [alphabet[i-step] for i in range(len(alphabet))]
        code_txt = ''.join([alphabet[code_alphabet.index(i_let)]
                            if i_let in alphabet else ' ' for i_let in i_line.lower()]).capitalize()+'\n'
        step += 1
        result += code_txt
    return result


def save():
    user_text = open('text.txt', 'r')
    data = user_text.read().split('\n')
    code_data = open('cipher_text.txt', 'w')
    code_data.write(code_text(data))
    code_data.close()
    user_text.close()


def print_file(file):
    text = open(file, 'r')
    data = text.read()
    print(f'Содержимое файла {file}:')
    print(data)


text = open('text.txt', 'w')
text.write('Hello\nHello\nHello\nHello')
text.close()
save()
print_file('text.txt')
print_file('cipher_text.txt')


