# Задача 7. Турнир
# В файле first_tour.txt записано число K и данные об участниках турнира по настольной игре «Орлеан»: фамилии, имена и
# количество баллов, набранных в первом туре. Во второй тур проходят участники, которые набрали более K баллов в первом
# туре. Напишите программу, которая выводит в файл second_tour.txt данные всех участников, прошедших во второй тур,
# с нумерацией. В первой строке нужно вывести в файл second_tour.txt количество участников второго тура. Затем программа
# должна вывести фамилии, инициалы и количество баллов всех участников, прошедших во второй тур, с нумерацией. Имя нужно
# сократить до одной буквы. Список должен быть отсортирован по убыванию набранных баллов.
# Пример:
# Содержимое файла first_tour.txt:
# 80
# Ivanov Serg 80
# Segeev Petr 92
# Petrov Vasiliy 98
# Vasiliev Maxim 78
# Содержимое файла second_tour.txt:
# 2
# 1) V. Petrov 98
# 2) P. Sergeev 92

def new_file_tour(file):
    file = open(file, 'r')
    data = file.read().split('\n')
    winners = list()
    result = ''
    for i_elem in data[1:]:
        tmp = i_elem.split(' ')
        if int(tmp[2]) > int(data[0]):
            winners.extend([[tmp[1][0].upper()+'.', tmp[0], tmp[2]]])
    winners.sort(key=take_elem, reverse=True)
    result += str(len(winners))+'\n'
    for i_elem in range(len(winners)):
        result += f'{i_elem+1}) ' + ' '.join(winners[i_elem])+'\n'
    return result


def take_elem(lst):
    return int(lst[2])


def print_file(file):
    text = open(file, 'r')
    data = text.read()
    print(f'Содержимое файла {file}:')
    print(data)


file = open('first_tour.txt', 'w')
data = '80\nIvanov Serg 80\nSegeev Petr 92\nPetrov Vasiliy 98\nVasiliev Maxim 78'
file.write(data)
file.close()

file = open('second_tour.txt', 'w')
file.write(new_file_tour('first_tour.txt'))
file.close()

print_file('first_tour.txt')
print_file('second_tour.txt')
